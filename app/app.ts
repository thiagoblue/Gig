import {Component} from '@angular/core';

import {TabsView} from './views/tabs';
import {LoginView} from './views/login';

import {HTTP_PROVIDERS} from '@angular/http';
import {Platform, ionicBootstrap} from 'ionic-angular';
import {StatusBar, Keyboard} from 'ionic-native';

import {RouterService, AccountService, APIRequest, AppNavigation, CartService} from './utils';

declare const navigator: any;

/** Main component to handle app screen. */
@Component({
  template: `
  <ion-nav [root]="router.getActiveView()"></ion-nav>
  
  <div id="gig_global_loading" style="display: none">
    <ion-spinner name="dots"></ion-spinner>
  </div>
  `,
})
export class MyApp {

  private rootPage: any;

  constructor(private platform: Platform,
              private router: RouterService,
              private account: AccountService,
              private api: APIRequest) {

    platform.ready().then(() => {
      // Set status bar style
      StatusBar.styleDefault();

      // Set keyboard style
      Keyboard.hideKeyboardAccessoryBar(false);

      // Set active platform
      if (this.platform.is('android')) {
        this.account.platform = 'android';
      } else if (this.platform.is('ios')) {
        this.account.platform = 'ios';
      } else if (this.platform.is('windows')) {
        this.account.platform = 'windows';
      }

      // Check login state and choose next screen
      if (this.account.isLoggedIn()) {
        this.router.setMainView(TabsView);
        
        // Refresh saved settings
        this.account.loadRemoteData();
      } else {
        this.router.setMainView(LoginView);
      }

      // Hide splash screen now
      if (navigator.splashscreen) {
        setTimeout(() => { navigator.splashscreen.hide(); }, 700); 
      }
    });
  }
}

ionicBootstrap(
  MyApp,
  [RouterService, AccountService, CartService, APIRequest, AppNavigation, HTTP_PROVIDERS],
  {
    prodMode: true,
  });