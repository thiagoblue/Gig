import {Component, ChangeDetectionStrategy, Pipe, PipeTransform} from '@angular/core';
import {NavController, NavParams, ModalController, Refresher, AlertController} from 'ionic-angular';
import {CartService, AccountService, APIRequest, Loading, ValuesPipe} from '../utils';

import {ProductDetailsView} from './shop/details';
import {AddressView} from './account/address';

declare const Zone: any;


/** Counts how many products there are inside a cart. */
@Pipe({
  name: 'countProducts',
  pure: false
})
export class CountProductsPipe implements PipeTransform {
  transform(value: any, args?: any[]): number {
    let count = 0;

    if (value) {
      for (const key of Object.keys(value)) {
        if (value[key]._amount) { count += value[key]._amount; }
      }
    }

    return count;
  }
}


/** Handle cart screen. */
@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Cart</ion-title>

    <ion-buttons start>
      <button [hidden]="!showSpecialBackButton" (click)="continueShopping()">
        <ion-icon class="back-button-icon" name="arrow-back"></ion-icon> Shop
      </button>
    </ion-buttons>

  </ion-navbar>
</ion-header>

<ion-content>

  <div class="view-empty" *ngIf="!cart.products">Your cart is empty.</div>

  <ion-list class="events-list cart-list" *ngIf="cart.products">

    <button ion-item class="event-item"
      *ngFor="let product of cart.products | values"
      (click)="details(product)">
      <ion-thumbnail item-left>
        <img src="{{product.imgUrl || 'assets/product_placeholder.png'}}">
      </ion-thumbnail>
      <h2 class="event-title">{{product.title}}</h2>
      
      <p *ngIf="!product._variants">Unique model</p>
      <p *ngFor="let variant of product._variants">{{variant.label}}: {{variant.value}}</p>
      
      <p>Qty: {{product._amount}}</p>
      <p>&#36;{{product.salePrice}} each</p>
    </button>

  </ion-list>

  <ion-list class="events-list cart-list" *ngIf="cart.products">
    <ion-item-divider>Delivery</ion-item-divider>
    
    <button ion-item (click)="editAddress()">
      <ion-icon item-left name="pin"></ion-icon>
      <ion-label *ngIf="userData.address">
        {{userData.address.address}}, {{userData.address.city}}, {{userData.address.state}}, {{userData.address.zip}}
      </ion-label>
      <ion-label *ngIf="!userData.address">Choose shipping address...</ion-label>
    </button>
    
    <!-- <button ion-item>
      <ion-icon item-left name="card"></ion-icon>
      <ion-label>Master ***</ion-label>
    </button> /-->

    <ion-item-divider>Cost</ion-item-divider>
    <ion-item>
      Subtotal
      <span>({{cart.products | countProducts}} items)</span>

      <div item-right>&#36;{{(cart.external && cart.external.total) || 0}}</div>
    </ion-item>
    
    <ion-item *ngIf="cart.external && cart.external.shippingCharge">
      Shipping <div item-right>&#36;{{cart.external && cart.external.shippingCharge}}</div>
    </ion-item>

    <ion-item no-lines>
      Tax <div item-right>&#36;{{cart.external && cart.external.tax || 0}}</div>
    </ion-item>

    <ion-item class="cart-total">
      Grand Total <div item-right>&#36;{{(cart.external && (cart.external.netTotal || cart.external.total)) || 0}}</div>
    </ion-item>

  </ion-list>

  <div class="product-purchase">
    <button class="green" [hidden]="!cart.products || (cart.external && cart.external.netTotal === 0)" large block (click)="order()">SUBMIT MY ORDER</button>
  </div>

</ion-content>
`,
  styles: [`
    .cart-total {
      font-weight: bold;
      border-top: 2pt solid black;
    }

    .back-button-icon {
      font-size: 3.4rem !important;
    }
  `],
  pipes: [ValuesPipe, CountProductsPipe]
})
export class CartView {

  cart: any;
  userData: any = {};
  showSpecialBackButton = false;

  constructor(private cartServ: CartService,
              private nav: NavController,
              private modal: ModalController,
              private api: APIRequest,
              private alert: AlertController,
              private account: AccountService) {
  }

  ngOnInit() {
    // Get user and cart data
    this.userData = this.account.getUserData();
    this.cart = this.cartServ.getCart();
  }

  ionViewWillEnter() {
    if (this.cart && this.cart.interface.hasJustAddedProduct) {
      // Show special button
      this.showSpecialBackButton = true;

      // Do not repeat
      this.cart.interface.hasJustAddedProduct = false;
    } else {
      // Hide special button
      this.showSpecialBackButton = false;
    }
  }

  empty() {
    // Empty the cart
    this.cartServ.reset();
  }

  details(product) {
    this.nav.push(ProductDetailsView, {
      item: { product },
      isEditingCart: true
    });
  }

  editAddress() {
    this.nav.push(AddressView);
  }

  order() {
    // Set shipping address for products
    const ref = this;
    const myzone = Zone.current;

    const address = this.userData.address;
    if (!address) {
      this.alert.create({
        title: 'Please choose your shipping address.',
        buttons: ['OK']
      }).present();
      
      // Go to address screen
      this.editAddress();
    } else if (address && !this.cart.external.shippingCharge) {
      // Shipping charge was not calculated yet
      this.alert.create({
        title: 'Could not calculate delivery cost for your cart. Please try again.',
        buttons: ['OK']
      }).present();

      this.cartServ.refresh();
    } else {
      Loading.showFullscreen();
      // Request order
      this.cartServ.checkout().then(
        res => {
          Loading.hideFullscreen();

          // Show confirm view
          const modal = this.modal.create(OrderConfirmView, { order: res });
          modal.present();

          // Go to account tab
          // @BUG disabled because it just makes this component stop detecting changes
          // this.nav.parent.select(2);
        },
        err => {
          Loading.hideFullscreen();
          this.alert.create({
            title: 'Could not finish your order. You were not charged.',
            subTitle: err,
            buttons: ['OK']
          }).present();
        }
      );
    }
  }

  continueShopping() {
    // Just go to event tab
    this.nav.parent.select(0);
  }

}



@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Order Confirmation</ion-title>
  </ion-navbar>
</ion-header>

<ion-content>

  <div class="order-final">
    <h1>Thank you!</h1>

    <h2>Your Order #</h2>
    <h2>{{(order && order.id) || 'Unavailable'}}</h2>

    <button block large (click)="close()">DONE</button>
  </div>

</ion-content>
  `
})
export class OrderConfirmView {

  order: any;
  constructor(private nav: NavController,
              private params: NavParams) {
    this.order = this.params.get('order');
  }

  close() {
    this.nav.pop();
  }

}