import {Component} from '@angular/core';
import {APIRequest, AppNavigation} from '../utils';

import {EventsView} from './event/search';
// import {AuctionView} from './auction';
// import {ShopView} from './shop/products';
import {CartView} from './cart';
import {AccountView} from './account/summary';

/** A view showing tabs for main app areas. */
@Component({
  template: `
  <ion-tabs>
    <ion-tab [root]="events" tabTitle="Events" tabIcon="list-box"></ion-tab>
    <!-- <ion-tab [enabled]="activeEvent.event" [root]="auction" tabTitle="Auction" tabIcon="flash"></ion-tab>
    <ion-tab [enabled]="activeEvent.event" [root]="shop" tabTitle="Shop" tabIcon="search"></ion-tab> /-->
    <ion-tab [root]="cart" tabTitle="Cart" tabIcon="cart"></ion-tab>
    <ion-tab [root]="account" tabTitle="Account" tabIcon="person"></ion-tab>
  </ion-tabs>
  `,
})
export class TabsView {

  private events = EventsView;
  // private auction = AuctionView;
  // private shop = ShopView;
  private cart = CartView;
  private account = AccountView;

  activeEvent: any;
  constructor(private api: APIRequest,
              private appNav: AppNavigation) {
    this.activeEvent = this.appNav.getActiveEvent();
  }

}