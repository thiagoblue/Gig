import {Component} from '@angular/core';
import {UpperCasePipe} from '@angular/common';
import {NavController, NavParams, AlertController} from 'ionic-angular';

import {APIRequest} from '../../utils';
import {ProductDetailsView} from '../shop/details';

declare const moment: any;
declare const cordova: any;

/** Interval to update auctions' bids, in miliseconds */
const UPDATE_AUCTION_INTERVAL = 5000;

/** Show events ordered by date, with details and possible actions. */
@Component({
  template: `
<ion-header class="events-header">
  <ion-navbar>
    <ion-title>{{barTitle}}</ion-title>
  </ion-navbar>

  <ion-toolbar>
    <ion-segment [(ngModel)]="section">
      <ion-segment-button value="details">Info</ion-segment-button>
      <ion-segment-button value="shop">Shop</ion-segment-button>
      <ion-segment-button value="auction">Auction</ion-segment-button>
    </ion-segment>
  </ion-toolbar>

</ion-header>

<ion-content [ngSwitch]="section">

  <!-- Details section /-->
  <ion-list class="events-list" *ngSwitchCase="'details'">

    <ion-item class="event-detail-photo">
      <img src="{{event.bgImgUrl || 'assets/event_placeholder.jpg'}}">
      <div class="event-overlay">
        <span class="when">{{event._dateLabel.w | uppercase}}<br>{{event._dateLabel.d}}<br>{{event._dateLabel.h}}</span>
        <span class="info">{{event.title}}<br>{{event.venue}}</span>
      </div>
    </ion-item>

    <ion-item-divider light *ngIf="event.bookingUrl">Tickets</ion-item-divider>
    <button ion-item *ngIf="event.bookingUrl" (click)="openTickets()">
      Buy now
      <!-- Buy on (some name) <ion-note item-right>from $(price here)</ion-note> /-->
    </button>

    <ion-item-divider light *ngIf="event.venue">Venue</ion-item-divider>
    <ion-item *ngIf="event.venue">
      <div class="event-detail-venue">{{event.venue}}</div>
      <div class="event-detail-address">{{event.street}}, {{event.city}}, {{event.state}}, {{event.zip}}</div>
      <div class="event-detail-map">
        <img src="https://maps.googleapis.com/maps/api/staticmap?zoom=12&size=300x100&scale=2&markers=|{{event.lat}},{{event.long}}&key=AIzaSyC4-QIf9zcChR2rmWvjEn2dgU_hNvd25NQ">
      </div>
    </ion-item>

  </ion-list>


  <!-- Auction section /-->
  <div class="auction-cards" *ngSwitchCase="'auction'">
    <div class="view-loading" *ngIf="isLoadingAuctions"><ion-spinner name="dots"></ion-spinner></div>
    <div class="view-empty" *ngIf="!isLoadingAuctions && auctions.length === 0">No Auctions Available</div>

    <ion-card *ngFor="let auction of auctions">
      <img src="{{auction.product.imgUrl || 'assets/product_placeholder.png'}}"/>
      <ion-card-content>
        
        <div class="auction-bid">
          <h1>
            <span *ngIf="auction._currentBid">Minimum bid &#36;{{auction._currentBidText}}</span>
            <span *ngIf="!auction.isUpdating && !auction._enabled">Auction ended.</span>
            <ion-spinner name="dots" [hidden]="!auction._isUpdating"></ion-spinner>
          </h1>
          <p [hidden]="!auction._enabled">Started at &#36;{{auction.startingPrice}}</p>

          <div class="auction-bid-status">
            <div [hidden]="auction._isLoadingBids">
              <h2 *ngIf="!bids[auction.id] && auction._enabled">You did not bid yet.</h2>
              <h2 *ngIf="!bids[auction.id] && !auction._enabled">You did not bid.</h2>
              <h2 *ngIf="bids[auction.id] && !auction._enabled">Your last bid was &#36;{{bids[auction.id].value}}</h2>
              <h2 *ngIf="bids[auction.id] && auction._enabled">Your bid is &#36;{{bids[auction.id].value}}</h2>

              <h3 *ngIf="bids[auction.id] && bids[auction.id].position == 1">You are the current high bidder!</h3>

              <div [class.disabled]="!auction._enabled">
                <input [disabled]="!auction._enabled" [(ngModel)]="nextBid" type="number" placeholder="{{'Type your bid. $'+(auction._currentBidText || 0)+' or more'}}" pattern="[0-9]*" min="{{auction._currentBid}}" step="1" />
                <button block [disabled]="!auction._enabled" (click)="bid(auction)">BID NOW</button>
              </div>
            </div>

            <ion-spinner name="dots" [hidden]="!auction._isLoadingBids"></ion-spinner>
          </div>
        </div>

        <ion-card-title>{{auction.product.title}}</ion-card-title>
        <p class="text">{{auction.product.excerpt}}</p>

        <br><p class="note">The product will be available after the show. You will be contacted via text message if you are the winner. All proceeds go to charity.</p>
      </ion-card-content>
    </ion-card>
  </div>


  <!-- Shop section /-->
  <div *ngSwitchCase="'shop'">

    <div class="view-loading" *ngIf="isLoadingProducts"><ion-spinner name="dots"></ion-spinner></div>
    <div class="view-empty" *ngIf="!isLoadingProducts && products.length === 0">No Products Available</div>

    <ion-list class="events-list" *ngIf="products.length > 0">
      <button ion-item class="event-item" *ngFor="let item of products" (click)="details(item)">
        <ion-thumbnail item-left>
          <img src="{{item.product.imgUrl || 'assets/product_placeholder.png'}}">
        </ion-thumbnail>
        <h2 class="event-title">{{item.product.title}}</h2>
        <p class="event-time">{{item.product._price}}</p>
      </button>
    </ion-list>

  </div>

</ion-content>
`,
  styles: [`
    .invisible {
      visibility: hidden;
    }

    .disabled {
      opacity: .3;
    }

    .auction-bid-status input {
      display: block;
      width: 100%;
      padding: 5pt;
      font-size: 1.2em;
      border: 1pt solid #eee;
    }

    p.text {
      color: black;
    }

    p.note {
      text-align: center;
    }

    .auction-bid-status {
      position: relative;
      margin: 10pt 0;
      background: #eee;
      border-radius: 5pt;
      padding: 10pt;
    }

    .auction-bid-status h2 {
      text-align: center;
      font-size: 1.4em;
      margin-bottom: 10pt;
    }
    
    .auction-bid-status h3 {
      text-align: center;
      color: white;
      background: #4CAF50;
      border-radius: 3pt;
      padding: 5pt;
      font-size: 1.2em;
      margin: 10pt 3vw;
      margin-bottom: 20pt;
    }

    .auction-bid-status ion-spinner {
      position: absolute;
      left: 0;
      right: 0;
      margin: auto;
      top: 0;
      bottom: 0;
    }

    h1 {
      position: relative;
    }

    h1 ion-spinner {
      vertical-align: sub;
      position: absolute;
      top: 0;
      right: 0;
    }
  `],
  pipes: [UpperCasePipe]
})
export class EventDetailsView {

  barTitle = 'Event';
  event;
  products = [];
  auctions = [];
  bids = {};
  nextBid: number;
  section = 'details';
  
  isLoading = false;
  isLoadingProducts = false;
  isLoadingAuctions = false;
  monitorAuctionsInterval;
  
  constructor(private nav: NavController,
              private api: APIRequest,
              private alert: AlertController,
              private params: NavParams) {
    this.nav = nav;
    this.params = params;

    // Get event data from previous view
    this.event = this.params.get('event');
  }

  ngOnInit() {
    // Load data
    this.loadProducts();
    this.loadAuctions();
  }

  ionViewWillEnter() {
    // Set title
    const title = this.event.title;
    if (title) {
      this.barTitle = title;
    }
  }

  openTickets() {
    // Open external browser to follow tickets' link
    if (this.event.bookingUrl) {
      cordova.InAppBrowser.open(this.event.bookingUrl, '_system');
      // window.open(this.event.bookingUrl, '_system');
    }
  }

  loadProducts() {
    // Search for products for current selected event
    this.products = [];
    
    if (this.event.id) {
      this.isLoadingProducts = true;

      this.api.call('GET', 'events/' + this.event.id + '/products', {
        page: 1,
        limit: 1000,
      })
      .then(res => {
        // Products found, proceed it there are results
        this.isLoadingProducts = false;

        if (res && res.data && res.data.length > 0) {
          // Extract products from server response
          const products = res.data;

          // For each product, add event data and add to results on screen
          const processedIds = [];
          for (const product of products) {
            // Skip product variation
            if (processedIds.indexOf(product.id) > -1) { continue; }

            // Check what price will be shown on screen
            product._price = (product.salePrice && ('$' + product.salePrice)) // Using fixed price
            || (product.hasVariation === '1' && ('from $' +
              Math.min( ...product.productVariantvalue.map(i => i.salePrice || i.basePrice) ).toFixed(2))) // The lowest price inside variations
            || ''; // Price unavailable

            this.products.push({
              product,
              event: this.event
            });
            processedIds.push(product.id);
          }
        }
      })
      .catch(err => {
        this.isLoadingProducts = false;

        this.alert.create({
          title: 'Could not load products for this event.',
          subTitle: err,
          buttons: ['OK']
        }).present();
      });
    }
  }

  loadAuctions() {
    // Search for auctions for current selected event
    this.auctions = [];
    clearInterval(this.monitorAuctionsInterval); 
    
    if (this.event.id) {
      this.isLoadingAuctions = true;

      this.api.call('GET', 'auctions', {
        page: 1,
        limit: 1000,
        sort: 'endsOn',
        order: 'desc',
        'filter[product][eventID]': this.event.id,
      })
      .then(res => {
        // Auctions found, proceed it there are results
        this.isLoadingAuctions = false;

        if (res && res.data && res.data.length > 0) {
          // Extract products from server response
          const auctions = res.data;

          // Add extra data
          for (const auction of auctions) {
            auction._isUpdating = false;
            auction._isLoadingBids = false;
            auction._enabled = true;
          }

          // Set auctions
          this.auctions = auctions;

          // Monitor auctions' prices
          this.updateAuctions();
          this.monitorAuctionsInterval = setInterval(() => { this.updateAuctions(); }, UPDATE_AUCTION_INTERVAL);
          
          // Updates user's bids once
          this.updateBids();
        }
      })
      .catch(err => {
        this.isLoadingAuctions = false;

        this.alert.create({
          title: 'Could not load auctions for this event.',
          subTitle: err,
          buttons: ['OK']
        }).present();
      });
    }
  }

  updateAuctions() {
    // Loads current bids for each auction
    if (this.auctions) {
      for (const auction of this.auctions) {
        if (!auction._error) {
          auction._isUpdating = true;

          // Only update enabled auctions
          if (auction._enabled) {
            // Update auction data
            this.api.call('GET', `auctions/${auction.id}/allowedMinBidAmountNow`).then(res => {
              // Update price
              const oldBid = parseFloat(auction._currentBid);

              auction._isUpdating = false;
              auction._currentBid = parseFloat(res);
              auction._currentBidText = parseFloat(res).toFixed(2);
              auction._enabled = true;

              // If value has changed, update my bids also
              if (oldBid !== auction._currentBid) { this.updateBids(); }
            }).catch(err => {
              // Stop this auction
              auction._isUpdating = false;
              auction._enabled = false;
              auction._error = err;
            });
          }
        }
      }
    }
  }

  updateBids() {
    // Updates all bids this user has done for each auction
    if (this.auctions) {
      for (const auction of this.auctions) {
        auction._isLoadingBids = true;
        this.api.call('GET', `auctions/${auction.id}/auctionBids`, {
          page: 1,
          limit: 1,
          sort: 'ts',
          order: 'desc'
        }).then(res => {
          // Save
          auction._isLoadingBids = false;
          
          const bidValue = res && res.data && res.data[0] && res.data[0].amt;
          const bidPosition = res && res.data && res.data[0] && res.data[0].position;
          if (bidValue) {
            this.bids[auction.id] = {
              value: bidValue,
              position: bidPosition,
            };
          }
        }).catch(err => {
          auction._isLoadingBids = false;
          // @TODO show error
        });
      }
    }
  }

  bid(auction) {
    // Add a bid for an auction
    if (auction && auction.id && this.nextBid) {
      // Update auction first
      this.api.call('GET', `auctions/${auction.id}/allowedMinBidAmountNow`).then(res => {
        // Check requirements
        if (res && res <= this.nextBid) {
          // Create bid
          this.api.call('POST', `auctions/${auction.id}/auctionBids`, { amt: this.nextBid }).then(res => {
            // Update user bids to confirm on the screen
            this.nextBid = null;
            this.updateBids();
          }).catch(err => {
            this.alert.create({
              title: 'Could not create your bid.',
              subTitle: err,
              buttons: ['OK']
            }).present();
          });
        } else {
          // Invalid bid value
          this.alert.create({
            title: 'Minimum bid has changed.',
            subTitle: `Insert a bid of ${res} or more.`,
            buttons: ['OK']
          }).present();
        }
      }).catch(err => {
        this.alert.create({
          title: 'This auction has ended.',
          subTitle: err,
          buttons: ['OK']
        }).present();
      });
    } else {
      this.alert.create({
        title: 'Insert a value for your bid.',
        buttons: ['OK']
      }).present();
    }
  }

  details(item) {
    // Show details view
    this.nav.push(ProductDetailsView, { item });
  }

}