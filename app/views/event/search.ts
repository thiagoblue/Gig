import {Component, ViewChild} from '@angular/core';
import {APIRequest, AccountService, AppNavigation} from '../../utils';
import {NavController, AlertController, InfiniteScroll, Refresher, ModalController} from 'ionic-angular';

import {EventDetailsView} from './details';
import {EventsFilterView} from './searchFilter';

declare const moment: any;
declare const Zone: any;
declare const isGigMapsLoaded: boolean;

/** Show events ordered by date, with details and possible actions. */
@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Events</ion-title>

    <ion-buttons start>
      <button [hidden]="isMapActive" [disabled]="isLoading" (click)="showMap()"><ion-icon name="md-map"></ion-icon></button>
      <button [hidden]="!isMapActive" (click)="hideMap()"><ion-icon name="ios-list"></ion-icon></button>
    </ion-buttons>

    <ion-buttons end>
      <button [disabled]="isLoading" (click)="showFilters()"><ion-icon name="ios-search"></ion-icon></button>
    </ion-buttons>

  </ion-navbar>
</ion-header>

<ion-content>

  <ion-refresher (ionRefresh)="reload(true)">
    <ion-refresher-content></ion-refresher-content>
  </ion-refresher>

  <div class="view-loading" [hidden]="(initiated && isRefreshing) || (initiated && !isLoading)"><ion-spinner></ion-spinner></div>

  <div class="view-empty" *ngIf="initiated && !isLoading && data.length === 0">Nothing found.</div>
  <ion-list class="events-list" [hidden]="isMapActive">

    <template ngFor let-day [ngForOf]="data">
      <ion-item-divider light>{{day.label}}</ion-item-divider>

      <button ion-item class="event-item" *ngFor="let event of day.items" (click)="details(event)">
        <ion-thumbnail item-left>
          <img src="{{event.thumbnailUrl || 'assets/event_placeholder.jpg'}}">
        </ion-thumbnail>
        <h2 class="event-title">{{event.title}}</h2>
        <p class="event-venue">{{event.venue}}</p>
        <p class="event-time">{{event._timeLabel}}</p>
        <p class="event-time" *ngIf="event._distance">{{event._distance}} miles</p>
      </button>
    </template>

  </ion-list>

  <ion-infinite-scroll (ionInfinite)="searchMore($event)">
    <ion-infinite-scroll-content></ion-infinite-scroll-content>
  </ion-infinite-scroll>

  <div id="events-map-container" [hidden]="!isMapActive">
    <div id="events-map-loading">loading map...</div>
  </div>
  <div class="map-loading" *ngIf="isMapActive" [hidden]="(initiated && isRefreshing) || (initiated && !isLoading)"><ion-spinner></ion-spinner></div>

</ion-content>
`,
  styles: [`
    #events-map-container {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
    }

    #events-map-loading {
      text-align: center;
      margin-top: 30vh;
      font-size: 2em;
    }

    .map-loading {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      margin: auto;
      z-index: 9;

      text-align: center;
      padding-top: 20vh;
      background: rgba(0, 0, 0, 0.87);
    }
  `]
})
export class EventsView {

  @ViewChild(InfiniteScroll) pageLoading: InfiniteScroll;
  @ViewChild(Refresher) refreshLoading: Refresher;

  initiated = false;
  isLoading = false;
  isRefreshing = false;
  rawData: Array<any> = [];
  data: Array<Object> = [];
  total: number = 0;

  isMapActive = true;
  isMapSet = false;
  isMapUpdated = false;
  map: google.maps.Map;
  mapMarkers: google.maps.Marker[] = [];
  mapConfig = { zoom: 3 };

  // Default search
  searchParams: any = {
    sort: 'startDtTime',
    order: 'asc',
    page: 1,
    limit: 30,
  };
  filters: any = {
    useMyLocation: true,
  };
  currentPosition = {lat: 39.8282, lng: -98.5795}; // Defaults to center of USA

  constructor(private nav: NavController,
              private alert: AlertController,
              private api: APIRequest,
              private modal: ModalController,
              private account: AccountService,
              private appNav: AppNavigation) {
  }

  ngOnInit() {
    // Load initial events
    if (!this.initiated) { this.reload(); }
  }

  /** Reload search from scratch. */
  reload(usingRefresherIndicator = false) {
    if (usingRefresherIndicator) { this.isRefreshing = true; }

    // Function to execute search when ready
    const zone = Zone.current;
    const ref = this;
    const runSearch = (position) => {
      // Run inside correct zone
      zone.run(() => {
        // Update search params
        const params = ref.searchParams;

        // Reset page
        params.page = 1;

        if ((ref.filters.useMyLocation || ref.filters.location) && position && position.lat && position.long) {
          // Add user location filter to search
          params['filter[lat][value]'] = position.lat;
          params['filter[long][value]'] = position.long;

          // Add radius
          if (ref.filters.distance) {
            params['distance'] = ref.filters.distance;
          }
        }

        // Search by event name
        if (ref.filters.name) {
          const words = ref.filters.name.split(' ');
          for (let i = 0; i < words.length; i++) { params[`filter[title][value][${i}]`] = `%${words[i]}%`; }
          params['filter[title][operator]'] = 'like';
        }

        // Search by event start date
        if (ref.filters.from && ref.filters.to) {
          params['filter[startDtTime][value][0]'] = `${ref.filters.from} 00:00:00`;
          params['filter[startDtTime][value][1]'] = `${ref.filters.to} 23:59:59`;
          params['filter[startDtTime][operator]'] = 'between';
        }
        
        // Reset page number
        params.page = 1;

        // Reset existing items
        ref.data = [];

        // Execute search
        ref.doSearch(params);
      });
    };

    // Make a search with user location or not
    if (this.filters.useMyLocation) {
      // Use user's position if possible
      this.account.getUserPosition()
      .then(position => { runSearch(position); })
      .catch(err => { runSearch(null); });
    } else if (this.filters.location && this.filters.location.lat && this.filters.location.long) {
      // Use a manual location
      runSearch(this.filters.location);
    } else {
      // Do search without location
      runSearch(null);
    }
    
  }

  /** Search for events based on given parameters. */
  doSearch(params) {
    // Function to stop loading indicators
    const finishLoading = () => {
      this.initiated = true;
      this.isLoading = false;
      this.isRefreshing = false;
      this.pageLoading.complete();
      this.refreshLoading.complete();
    };

    this.isLoading = true;
    this.api.call('GET', 'events', params)
    .then(events => {
      // Finish loading
      finishLoading();

      // Disable infinite scroll if there is no more results
      if (events.total && events.total <= this.searchParams.page * this.searchParams.limit) {
        this.pageLoading.enable(false);
      } else { this.pageLoading.enable(true); }
      
      // Process data
      // DEBUG this.data = this.getTestData();
      this.data = this.data.concat(this.processEvents(events.data));

      // Update maps view if active
      if (this.isMapActive) { this.showMap(); }
    })
    .catch((err) => {
      finishLoading();
      this.alert.create({
        title: 'Could not load events',
        subTitle: err,
        buttons: ['OK']
      }).present();
    });
  }

  /** Search next page of events.
   * @param indicator - Receives the loading indicator from Ionic. */
  searchMore(indicator) {
    // Add paging number
    this.searchParams.page++;

    // Execute search
    this.doSearch(this.searchParams);
  }

  /** Transform database events into a list grouped by date */
  processEvents(events) {
    // Order items by date group
    const groups = {};
    for (const event of events) {
      // Put event into correct group, by date
      const info = event.startDtTime && event.startDtTime.split(' ');
      const group = info[0];
      if (group) {
        if (!groups[group]) { groups[group] = []; }

        // Prepare event's distance, if any
        if (event.distance) { event._distance = Math.round(event.distance); }

        // Add hour label to event
        event._hour = info[1];
        event._timeLabel = moment(info[1], 'hh:mm:ss').format('LT');
        
        const full = moment(event.startDtTime); 
        event._dateLabel = {
          w: full.format('ddd'),
          d: full.format('MMM D'),
          h: full.format('h:mm A'),
        };
        groups[group].push(event);
      }
    }

    // Store raw data
    this.rawData = events;
    this.isMapUpdated = false; // Map view will be reloaded next time

    // Return groups as a ordered list
    const res = [];
    for (const group of Object.keys(groups)) {
      // Order events inside a day, by time
      groups[group].sort((a, b) => {
        if (a._hour < b._hour) { return -1; }
        if (a._hour > b._hour) { return 1; }
        return 0;
      });

      res.push({
        date: group,
        label: moment(group).calendar(null, {
          sameDay: '[Today]',
          nextDay: '[Tomorrow]',
          nextWeek: '[Next] dddd, MMM Do',
          sameElse: 'dddd, MMMM Do',

          lastDay: '[Yesterday]',
          lastWeek: '[Last] dddd, MMM Do',
        }),
        items: groups[group]
      });
    }

    // Order groups by date
    res.sort((a, b) => {
      if (a.date < b.date) { return -1; }
      if (a.date > b.date) { return 1; }
      return 0;
    });

    return res;
  }

  /** Show screen with event details */
  details(event) {
    this.nav.push(EventDetailsView, { event });

    // Set global selected event for app
    this.appNav.setActiveEvent(event);
  }

  ionViewWillEnter() {
    // Remove selected event
    this.appNav.setActiveEvent(null);
  }

  /** Show map events' view */
  showMap() {
    this.isMapActive = true;
    this.refreshLoading.enabled = false;

    // Activate map plugin
    if (!this.isMapSet) {
      if (!isGigMapsLoaded) {
        // Map is still loading, try again later
        console.log('map not loaded. trying again later.');
        setTimeout(() => {
          // Only proceed if map mode is still selected
          if (this.isMapActive) { this.showMap(); }
        }, 500);
      } else {
        // Create a new map
        this.map = new google.maps.Map(document.getElementById('events-map-container'), {
          center: this.currentPosition,
          zoom: this.mapConfig.zoom,
          zoomControl: false,
          mapTypeControl: false,
          scaleControl: false,
          streetViewControl: false,
        });

        this.isMapSet = true;
      }
    }

    // Update map if needed
    if (!this.isMapUpdated) {
      // Remove all current markers
      for (const marker of this.mapMarkers) { marker.setMap(null); }
      this.mapMarkers = [];

      // Add markers
      for (const event of this.rawData) {
        if (event.lat && event.long) {
          // Setup marker
          const aMarker = new google.maps.Marker({
            position: { lat: parseFloat(event.lat), lng: parseFloat(event.long) },
            map: this.map,
            animation: google.maps.Animation.DROP
          });

          // Setup info popup
          const infoContent = document.createElement('div');
          infoContent.innerHTML = `
            <div class="event-map-info">
              <div class="gig-emi-title">${event.title || 'Event'}</div>
              <div class="gig-emi-venue">${event.venue || event.street || ''}</div>
              <div class="gig-emi-date">${(event._dateLabel &&
                (event._dateLabel.w + ', ' + event._dateLabel.d + ', ' + event._dateLabel.h)) || ''}</div>
              <div class="gig-emi-button">Tap to see more.</div>
            </div>
          `;

          // Add info popup click listener
          infoContent.addEventListener('click', (ev) => {
            // Popup was clicked. Show details screen from the scoped event variable
            this.details(event);
          });

          // Create info popup object
          const aInfo = new google.maps.InfoWindow({
            content: infoContent
          });
          let isInfoShown = false; // Store info window state in the scope
          aMarker.addListener('click', () => {
            // Marker was clicked. Toggle info popup
            if (!isInfoShown) {
              aInfo.open(this.map, aMarker);
              isInfoShown = true;
            } else {
              aInfo.close();
              isInfoShown = false;
            }
          });

          // Add marker
          this.mapMarkers.push(aMarker);
        }
      }

      // Set map bounds based on current markers
      if (this.mapMarkers.length > 0) {
        const bounds = new google.maps.LatLngBounds();
        for (const marker of this.mapMarkers) {
          // Extend bounds
          bounds.extend(marker.getPosition());
        }

        // Update map bounds when possible
        setTimeout(() => {
          this.map.fitBounds(bounds);
        }, 500);
      } else {
        // Reset map
        this.map.setCenter(this.currentPosition);
        this.map.setZoom(this.mapConfig.zoom);
      }

      this.isMapUpdated = true;
    }
  }

  /** Hide map view. */
  hideMap() {
    this.isMapActive = false;
    this.refreshLoading.enabled = true;
  }

  /** Open search settings. */
  showFilters() {
    // Show screen as modal
    this.modal.create(EventsFilterView, {
      filters: this.filters,
      callback: (filters) => {
        // Reload search using filters
        this.searchParams = {
          sort: 'startDtTime',
          order: 'asc',
          page: 1,
          limit: 30,
        };
        this.filters = filters;
        this.reload();
      }
    }).present();
  }

}