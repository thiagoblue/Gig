import {Component} from '@angular/core';
import {Http} from '@angular/http';
import {NavController, NavParams, ModalController, LoadingController} from 'ionic-angular';

/** Set filters to event search. */
@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Search</ion-title>

    <ion-buttons start>
      <button (click)="close()">Cancel</button>
    </ion-buttons>

    <ion-buttons end>
      <button (click)="reset()">Reset</button>
    </ion-buttons>

  </ion-navbar>
</ion-header>

<ion-content>
  
  <ion-list>
    <ion-list-header>By Event Name</ion-list-header>
    <ion-item no-lines class="lined">
      <ion-label floating>Search event name</ion-label>
      <ion-input type="text" [(ngModel)]="filters.name"></ion-input>
    </ion-item>

    <ion-list-header>By Location</ion-list-header>
    <ion-item no-lines>
      <ion-label>Near my current location</ion-label>
      <ion-toggle [(ngModel)]="filters.useMyLocation" (click)="selectLocation()"></ion-toggle>
    </ion-item>

    <ion-item no-lines class="hiddeable" [class.collapse]="filters.useMyLocation" (click)="selectLocation(true)">
      <ion-label>Near {{filters.locationLabel || 'another location'}}</ion-label>
      <ion-icon item-right name="md-create" class="location-edit"></ion-icon>
    </ion-item>

    <ion-item no-lines class="lined">
      <ion-label class="location">Distance up to {{filters.distance}} miles</ion-label>
      <ion-range [(ngModel)]="filters.distance" min="5" max="100" step="5">
        <ion-icon range-left name="walk"></ion-icon>
        <ion-icon range-right name="car"></ion-icon>
      </ion-range>
    </ion-item>

    <ion-list-header>By Date</ion-list-header>
    <ion-item>
      <ion-label>From</ion-label>
      <ion-datetime yearValues="2016,2017,2018,2019,2020" [class.overwriteTodayDateFormat]="shouldOverwriteFromDate" displayFormat="DDDD, MMMM D, YYYY" pickerFormat="MMMM D YYYY" [(ngModel)]="filters.from" (ngModelChange)="adjustDates()"></ion-datetime>
    </ion-item>
    <ion-item>
      <ion-label>To</ion-label>
      <ion-datetime yearValues="2016,2017,2018,2019,2020" [class.overwriteTodayDateFormat]="shouldOverwriteToDate" displayFormat="DDDD, MMMM D, YYYY" pickerFormat="MMMM D YYYY" [(ngModel)]="filters.to" (ngModelChange)="checkDateLabels()" [min]="filters.from"></ion-datetime>
    </ion-item>
  
  </ion-list>

  <div class="padding">
    <button block large (click)="save()">SEARCH</button>
  </div>

</ion-content>
`,
  styles: [`
    .padding {
      padding: 0 10pt;
    }

    .lined {
      border-bottom: 1px solid #c8c7cc;
      margin-bottom: 10pt;
    }

    .location {
      margin-top: 15pt;
      margin-bottom: 0;
    }

    .location-edit {
      margin-left: 10pt;
    }

    .hiddeable {
      transition: all linear 200ms;
      height: 1em;
      text-align: center;
      font-weight: bold;
    }

    .collapse {
      height: 0;
      min-height: 0;
    }
    
  `]
})
export class EventsFilterView {

  filters: any = {};
  defaults: any = {
    distance: 50,
    useMyLocation: true,
  };

  // Modifies on screen labels to dates
  shouldOverwriteFromDate = true;
  shouldOverwriteToDate = false;
  
  constructor(private nav: NavController,
              private modal: ModalController,
              private params: NavParams) {
    // Set filter defaults for dates related to today
    this.defaults.from = new Date();

    const future = new Date();
    future.setDate(future.getDate() + 2);
    this.defaults.to = future;
  }

  ngOnInit() {
    // Restore used filters
    const filters = this.params.get('filters');
    if (filters) { this.filters = filters; }

    // Set missing defaults
    if (typeof this.filters.useMyLocation === 'undefined') { this.filters.useMyLocation = Boolean(this.defaults.useMyLocation); }
    if (!this.filters.distance) { this.filters.distance = Number(this.defaults.distance); }
    if (!this.filters.from) { this.filters.from = new Date(this.defaults.from).toISOString().split('T')[0]; }
    if (!this.filters.to) { this.filters.to = new Date(this.defaults.to).toISOString().split('T')[0]; }
  }

  save() {
    // Send filters to callback, if any
    const callback = this.params.get('callback');
    if (callback) { callback(this.filters); }

    // close
    this.nav.pop();
  }

  reset() {
    // Set values back to a default
    this.filters = {
      distance: Number(this.defaults.distance),
      from: null,
      to: null,
      useMyLocation: Boolean(this.defaults.useMyLocation),
      location: null,
      name: null,
    };
  }

  close() {
    this.nav.pop();
  }

  selectLocation(forceEdit = false) {
    // Open search screen if first time
    if (this.filters.useMyLocation === false && (forceEdit || !this.filters.locationLabel)) {
      this.modal.create(LocationSearch, {
        callback: (position, label) => {
          if (position && label) {
            // Use chosen position and label
            this.filters.locationLabel = label;
            this.filters.location = {
              lat: position.lat,
              long: position.lng
            };
          } else {
            // Cancel manual location
            if (!this.filters.locationLabel) { this.filters.useMyLocation = true; }
          }
        }
      }).present();
    }
  }

  adjustDates() {
    // "Date to" must be always later than "date from"
    if (this.filters.from > this.filters.to) {
      // Set "to" to 2 days later
      const targetDate = new Date(this.filters.from);
      targetDate.setDate(targetDate.getDate() + 2);
      
      this.filters.to = targetDate.toISOString().split('T')[0];
    }

    this.checkDateLabels();
  }

  checkDateLabels() {
    // If a date is today, activate a special class to replace the label on the screen
    const today = new Date().toISOString().split('T')[0];
    this.shouldOverwriteFromDate = (this.filters.from === today);
    this.shouldOverwriteToDate = (this.filters.to === today);
  }

}


/** Search for locations using Google Places Autocomplete */
@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Choose a Location</ion-title>

    <ion-buttons start>
      <button (click)="close()">Cancel</button>
    </ion-buttons>

  </ion-navbar>
</ion-header>

<ion-content padding>
  
  <ion-list class="gig-form">
    <ion-item no-lines>
      <ion-input type="text" [(ngModel)]="query" (ngModelChange)="suggestLocation()" placeholder="Type a city"></ion-input>
    </ion-item>
  </ion-list>

  <ion-list class="gig-form results">
    <ion-item *ngFor="let place of results" (click)="chooseLocation(place)">{{place.label}}</ion-item>
  </ion-list>

</ion-content>
`})
class LocationSearch {

  query: string;
  results: any[] = [];
  callback: any;

  isSearchScheduled: boolean = false;
  constructor(private nav: NavController,
              private http: Http,
              private loading: LoadingController,
              private params: NavParams) {}

  suggestLocation() {
    // Get suggestions from google places
    
    // Only if a search is not scheduled
    if (!this.isSearchScheduled) {
      // Ignore all keystrokes for the next miliseconds, then search
      this.isSearchScheduled = true;
      setTimeout(() => {
        // Request suggestions
        this.http.get(`https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyC4-QIf9zcChR2rmWvjEn2dgU_hNvd25NQ&language=en_US&type=(cities)&input=${ encodeURIComponent(this.query) }`)
        .subscribe(
          res => {
            this.isSearchScheduled = false;

            // Process results
            const results = res.json();
            
            this.results = [];
            if (results.predictions) {
              for (const item of results.predictions) {
                this.results.push({
                  label: item.description,
                  id: item.place_id
                });
              }
            }
          },
          err => {
            this.isSearchScheduled = false;
            console.log('places api error was', err);
            alert('Could not connect to suggestions API');
          }
        );
      }, 500);
    }
  }

  chooseLocation(place) {
    // Find GPS position and use this location
    if (place && place.id) {
      const loading = this.loading.create();
      loading.present();

      this.http.get(`https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyC4-QIf9zcChR2rmWvjEn2dgU_hNvd25NQ&placeid=${ encodeURIComponent(place.id) }`)
      .subscribe(
        res => {
          loading.dismiss().then(() => {
            // Extract data
            const data = res.json();
            const position = data && data.result && data.result.geometry && data.result.geometry.location;

            // Return location name and GPS position
            const callback = this.params.get('callback');
            if (callback) { callback(position, place.label); }

            // Close modal
            this.nav.pop();
          });
        },
        err => {
          loading.dismiss();
          console.log('places api error was', err);
          alert('Could not find GPS position for this location.');
        }
      );
    }
  }

  close() {
    // Callback with cancelling
    const callback = this.params.get('callback');
    if (callback) { callback(null, null); }

    // Close modal
    this.nav.pop();
  }

}