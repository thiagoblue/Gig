import {Component} from '@angular/core';
import {NavController, NavParams, AlertController} from 'ionic-angular';
import {CartService, AppNavigation, Loading} from '../../utils';

declare const moment: any;

@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Product Details</ion-title>
  </ion-navbar>
</ion-header>

<ion-content>

  <ion-card>
    <img src="{{item.product.imgUrl || 'assets/product_placeholder.png'}}"/>
    
    <ion-card-content>
      <ion-card-title *ngIf="item.product.title">{{item.product.title}}</ion-card-title>
      <p *ngIf="item.product.excerpt">{{item.product.excerpt}}</p>
    </ion-card-content>

      <ion-list>
        <!-- <ion-item-divider light>{{item.product._price}} each</ion-item-divider> /-->
        <ion-item class="product-item" *ngFor="let option of item._options">
          
          <div class="product-item-price">
            <span class="label left">
              <div class="product-item-variant" *ngFor="let variant of option._variants">
                <em>{{variant.value}}</em> <span>{{variant.label}}</span> 
              </div>
              <span *ngIf="!option._variants">One Size/Type Available</span>
            </span>
            
            <span class="cart right">
              <button [disabled]="option._amount === 0" (click)="changeItem(option, -1)">
                <ion-icon name="md-remove-circle"></ion-icon>
              </button>

              <span class="amount">{{option._amount}}</span>

              <button [disabled]="!option._enabled" (click)="changeItem(option, 1)">
                <ion-icon name="md-add-circle"></ion-icon>
              </button>
            </span>
          </div>

          <div class="product-item-info">
            <span class="value left">&#36;{{option.salePrice}}</span>
            <span class="status right" [class.warning]="option._enabled === false">{{option.stockStatus}}</span>
          </div>
        </ion-item>
      </ion-list>

  </ion-card>

  <div class="product-purchase">
    <button [disabled]="!isPresentInCart && totalItems <= 0" large block (click)="toCart()">{{(isPresentInCart && 'UPDATE CART') || 'ADD TO CART'}}</button>
  </div>

</ion-content>
`,
styles: [`
  .invisible {
    visibility: hidden;
  }

  .warning {
    color: #e13838;
  }

  .product-item-price, .product-item-info {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .product-item .left, .product-item .right {
    flex: 1 1 auto;
    white-space: normal;
  }

  .product-item .left > *, .product-item .right > * {
    vertical-align: middle;
  }

  .product-item .right {
    text-align: right;
    flex: 0 0 auto;
    margin-left: 10pt;
  }

  .product-item-price {
    margin-bottom: 10pt;
  }

  .product-item-info .value {
    font-size: 1.3em;
  }

  .product-item-price .cart button {
    height: 30pt;
    padding: 5pt;
  }

  .product-item-price .amount {
    font-size: 2em;
    width: 2em;
    display: inline-block;
    text-align: center;
  }

  .product-item-variant {
    font-size: .9em;
    text-transform: lowercase;
    color: #999;
  }

  .product-item-variant em {
      font-size: 1.5em;
      font-style: normal;
      font-weight: bold;
      color: black;
  }

  .product-item-variant span {
    margin-left: 5pt;
  }
`]
})
export class ProductDetailsView {

  item: any;
  cart: any;
  isEditingCart: boolean;
  isPresentInCart = false;
  canAddToCart = false;
  totalItems = 0;

  constructor(private params: NavParams,
              private nav: NavController,
              private cartServ: CartService,
              private appNav: AppNavigation,
              private alert: AlertController) {
    this.params = this.params;
    this.cart = this.cartServ.getCart();

    // Get event data from previous view
    this.item = JSON.parse(JSON.stringify(this.params.get('item')));
    this.isEditingCart = this.params.get('isEditingCart');

    // Set default values if needed
    if (!this.item) { this.item = {}; }
    if (!this.item.product) { this.item.product = {}; }
    if (!this.item.event) { this.item.event = {}; }

    const getInternalCartID = (product) => {
      return product._cartID + (product._extraCartID && `_${product._extraCartID}` || '');
    };

    // Organize purchase options for item
    if (this.item.product.hasVariation === '1' && this.item.product.productVariantvalue) {
      // Include all variations as purchase options
      this.item._options = this.item.product.productVariantvalue;
    } else {
      // Add itself as only option
      this.item._options = [this.item.product];
    }
    
    // Check additional data for each option
    for (const option of this.item._options) {
      // Check if it is available or not
      if (!option._amount) { option._amount = 0; }

      if (!option._enabled) {
        option._enabled = (option.stockStatus === 'out-of-stock' ||
          option.stockStatus === 'discontinued') ? false : true;
      }
      
      // Add ID to cart API
      if (!option._cartID) { option._cartID = this.item.product.id; }

      // Add original product data
      if (!option.title) { option.title = this.item.product.title; }
      if (!option.imgUrl) { option.imgUrl = this.item.product.imgUrl; }

      // Add variations if any
      if (!option._variants && option.variantValueID) {
        option._variants = [];
        for (const variant of Object.keys(option.variantValueID)) {
          option._variants.push({
            label: option.variantValueID[variant].variant.title,
            value: option.variantValueID[variant].title
          });
        }

        // Build cart ID for this product
        option._extraCartID = option.id;
      }

      // Sync with current cart
      if (this.cart.products && this.cart.products[ getInternalCartID(option) ]) {
        // Enable editing mode because product already exists in the cart
        this.isPresentInCart = true;
        const inCart = this.cart.products[ getInternalCartID(option) ];

        option._amount = inCart._amount;
        option._updateID = inCart._updateID;
      }
    }
    
  }

  changeItem(item, increment = 1) {
    // Add or remove item to add to cart later
    item._amount += increment;
    this.totalItems += increment;
  }

  toCart() {
    // Add current item to cart

    // Check if quantity was chosen
    let isSet = false;
    for (const option of this.item._options) {
      if (option._amount > 0) {
        isSet = true;
        break;
      }
    }

    if (isSet || this.isPresentInCart) {
      Loading.showFullscreen();

      this.cartServ.setProduct(this.item).then(
        res => {
          // Change screen
          Loading.hideFullscreen();

          if (!this.isEditingCart) {
            // Show special button when cart is shown
            if (this.cart) { this.cart.interface.hasJustAddedProduct = true; }

            // Go to cart tab
            this.nav.parent.select(1);
          }

          // Close details
          this.nav.pop();
        },
        err => {
          Loading.hideFullscreen();
          // Show error
          this.alert.create({
            title: 'Could not add this product to cart.',
            subTitle: err,
            buttons: ['OK']
          }).present();
        }
      );
    } else {
      this.alert.create({
        title: 'Please add an item first.',
        subTitle: 'Tap the plus sign.',
        buttons: ['OK']
      }).present();
    }
  }

}