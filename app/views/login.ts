import {Component} from '@angular/core';
import {ModalController, NavController} from 'ionic-angular';

import {AccountService} from '../utils';

@Component({
  template: `
  <ion-navbar></ion-navbar>

  <ion-content class="login-background">
    <div class="flex-container">
      <div class="login-title">
        <img src="assets/logo_small.png" class="login-logo">
        <div>At The Gig</div>
      </div>
      
      <div class="login-menu">
        <button block large danger (click)="login('google')">
          <span class="button-fixed-icon"><ion-icon name="logo-googleplus"></ion-icon></span>
          <span class="button-fixed-label">Log in with Google</span>
        </button>

        <button block large (click)="login('facebook')">
          <span class="button-fixed-icon"><ion-icon name="logo-facebook"></ion-icon></span>
          <span class="button-fixed-label">Log in with Facebook</span>
        </button>

        <button block large favorite (click)="openEmailLogin()">
          <span class="button-fixed-icon"><ion-icon name="mail"></ion-icon></span>
          <span class="button-fixed-label">Log in with Email</span>
        </button>

        <!--<button block large danger (click)="login('DEBUG')">
          <span class="button-fixed-icon"><ion-icon name="logo-googleplus"></ion-icon></span>
          <span class="button-fixed-label">TEST</span>
        </button>-->
      </div>
    </div>
  </ion-content>
  `,
  styles: [`
    .flex-container {
      -webkit-justify-content: space-around;
      justify-content: space-around;
    }

    .login-background {
      background-color: black;
      background-image: url('assets/login_bg1.jpg');
      background-repeat: no-repeat;
      background-size: cover;
      background-position: center;
    }

    .login-title {
      font-size: 2.5em;
      color: white;
      font-weight: bold;
      text-align: center;
    }

    .login-logo {
      height: 30vh;
      margin-bottom: 10pt;
    }

    .login-menu {
      padding: 5pt 15pt;
      width: auto;
    }

    .login-menu button {
      margin: 7pt 0;
    }
  `],
})
export class LoginView {

  constructor(private account: AccountService,
              private nav: NavController) {
  }

  login(type: string) {
    this.account.login(type);
  }

  openEmailLogin() {
    this.nav.push(LoginEmailView);
  }

}



@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Login</ion-title>
  </ion-navbar>
</ion-header>

<ion-content padding>
  <ion-list class="gig-form">

    <ion-item>
      <ion-label fixed>Email</ion-label>
      <ion-input type="email" [(ngModel)]="email"></ion-input>
    </ion-item>

    <ion-item>
      <ion-label fixed>Password</ion-label>
      <ion-input type="password" [(ngModel)]="password"></ion-input>
    </ion-item>

  </ion-list>

  <button danger block large (click)="login()">Log in</button>
  
  <br><br>
  <button clear block (click)="register()">Create an account</button>
</ion-content>
  `
})
export class LoginEmailView {

  email;
  password;
  constructor(private account: AccountService,
              private nav: NavController) {
  }

  login() {
    this.account.login('email', {
      email: this.email,
      password: this.password
    });
  }

  register() {
    this.nav.push(RegisterView);
  }

}


@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Sign up</ion-title>
  </ion-navbar>
</ion-header>

<ion-content padding>

  <ion-list class="gig-form">
    <ion-item>
      <ion-label fixed>First Name</ion-label>
      <ion-input type="text" [(ngModel)]="firstName"></ion-input>
    </ion-item>

    <ion-item>
      <ion-label fixed>Last Name</ion-label>
      <ion-input type="text" [(ngModel)]="lastName"></ion-input>
    </ion-item>
  </ion-list>

  <ion-list class="gig-form">
    <ion-item>
      <ion-label fixed>Email</ion-label>
      <ion-input type="email" [(ngModel)]="email"></ion-input>
    </ion-item>

    <ion-item>
      <ion-label fixed>Password</ion-label>
      <ion-input type="password" [(ngModel)]="password"></ion-input>
    </ion-item>
  </ion-list>

  <button danger block large (click)="register()">Sign up</button>
</ion-content>
  `
})
export class RegisterView {

  firstName;
  lastName;
  email;
  password;
  constructor(private account: AccountService,
              private nav: NavController) {
  }

  register() {
    this.account.login('email', {
      register: true,
      email: this.email,
      password: this.password,
      firstName: this.firstName,
      lastName: this.lastName,
    });
  }

}