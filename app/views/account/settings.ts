import {Component} from '@angular/core';
import {NavController, AlertController} from 'ionic-angular';

import {AccountService, APIRequest, Loading} from '../../utils';

import {PasswordView} from './password';

@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Settings</ion-title>
    
    <ion-buttons end>
      <button (click)="save()">Save</button>
    </ion-buttons>

  </ion-navbar>
</ion-header>

<ion-content>

  <ion-list>
    <ion-list-header>Privacy</ion-list-header>
    <ion-item no-lines>
      <ion-label>Show Purchases in Trends</ion-label>
      <ion-toggle item-right [(ngModel)]="settings.privacy.showPurchaseInTrends"></ion-toggle>
    </ion-item>


    <ion-list-header>Notifications</ion-list-header>
    <ion-item>
      <ion-label>Near a Venue</ion-label>
      <ion-toggle item-right [(ngModel)]="settings.notification.nearVenue"></ion-toggle>
    </ion-item>

    <ion-item>
      <ion-label>Auction Ending Soon</ion-label>
      <ion-toggle item-right [(ngModel)]="settings.notification.auctionEndingSoon"></ion-toggle>
    </ion-item>

    <ion-item>
      <ion-label>Auction Outbid</ion-label>
      <ion-toggle item-right [(ngModel)]="settings.notification.auctionOutbid"></ion-toggle>
    </ion-item>

    <ion-item no-lines>
      <ion-label>Auction Won</ion-label>
      <ion-toggle item-right [(ngModel)]="settings.notification.auctionWon"></ion-toggle>
    </ion-item>


    <ion-list-header>Account</ion-list-header>
    
    <button ion-item (click)="password()">
      Update Password
    </button>

    <button ion-item danger (click)="logout()">
      Log Out
    </button>
  </ion-list>

</ion-content>
`,
})
export class SettingsView {

  settings: any = {
    privacy: {},
    notification: {},
  };
  constructor(private account: AccountService,
              private api: APIRequest,
              private alert: AlertController,
              private nav: NavController) {
    // Load saved Settings
    const data = this.account.getUserData();
    if (data && data.settings) {
      // Use a copy
      this.settings = JSON.parse(JSON.stringify(data.settings));
    }
  }

  logout() {
    this.account.logout();
  }

  password() {
    this.nav.push(PasswordView);
  }

  save() {
    // Save changes
    Loading.showFullscreen();
    this.api.call('PUT', 'userSettings', {
      privacy: this.settings.privacy,
      notification: this.settings.notification
    }).then(res => {
      // Store settings and go back
      Loading.hideFullscreen();
      
      this.account.saveUserData({ settings: this.settings });
      this.nav.pop();
    }).catch(err => {
      this.alert.create({
        title: 'Could not save your preferences.',
        subTitle: err,
        buttons: ['OK'],
      }).present();
    });
  }

}
