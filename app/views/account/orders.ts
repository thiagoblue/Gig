import {Component, ViewChild} from '@angular/core';
import {APIRequest} from '../../utils';
import {NavController, AlertController, InfiniteScroll, Refresher} from 'ionic-angular';

import {OrderDetailView} from './orderDetail';

declare const moment: any;

@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Order History</ion-title>
  </ion-navbar>
</ion-header>

<ion-content>

  <ion-refresher (ionRefresh)="reload()">
    <ion-refresher-content></ion-refresher-content>
  </ion-refresher>

  <div class="view-loading" [hidden]="initiated"><ion-spinner></ion-spinner></div>

  <div class="view-empty" *ngIf="initiated && !isLoading && data.length === 0">Nothing found.</div>
  <ion-list class="events-list">

    <template ngFor let-day [ngForOf]="data">
      <ion-item-divider light>{{day.label}}</ion-item-divider>

      <button ion-item class="event-item" *ngFor="let order of day.items" (click)="details(order)">
        <ion-thumbnail item-left>
          <img src="{{order._summaryImg || 'assets/product_placeholder.png'}}">
        </ion-thumbnail>
        <h2 class="event-title">Order #{{order.id}}</h2>
        
        <p class="event-venue">&#36;{{order._value}}</p>
        <p class="event-time">Items: {{order._itemsAmount}}</p>

        <p class="event-time" *ngIf="order.orderStatus">Status: {{order.orderStatus}}</p>
        <p class="event-time" *ngIf="order.paymentStatus">Payment: {{order.paymentStatus}}</p>
      </button>
    </template>

  </ion-list>

  <ion-infinite-scroll (ionInfinite)="searchMore($event)">
    <ion-infinite-scroll-content></ion-infinite-scroll-content>
  </ion-infinite-scroll>

</ion-content>
`,
})
export class OrdersView {

  @ViewChild(InfiniteScroll) pageLoading: InfiniteScroll;
  @ViewChild(Refresher) refreshLoading: Refresher;

  initiated = false;
  isLoading = false;
  data: Array<Object> = [];
  total: number = 0;

  // Default search
  searchParams: any = {
    sort: 'ts',
    order: 'desc',
    page: 1,
    limit: 30,
  };

  constructor(private nav: NavController,
              private alert: AlertController,
              private api: APIRequest) {
  }

  ngOnInit() {
    // Load initial events
    if (!this.initiated) { this.reload(); }
  }

  /** Reload search from scratch. */
  reload() {
    this.data = [];
    this.searchParams.page = 1;
    this.doSearch(this.searchParams);
  }

  /** Search for events based on given parameters. */
  doSearch(params) {
    // Function to stop loading indicators
    const finishLoading = () => {
      this.initiated = true;
      this.isLoading = false;
      this.pageLoading.complete();
      this.refreshLoading.complete();
    };

    this.isLoading = true;
    this.api.call('GET', 'orders', params)
    .then(orders => {
      // Finish loading
      finishLoading();

      // Disable infinite scroll if there is no more results
      if (orders.total && orders.total <= this.searchParams.page * this.searchParams.limit) {
        this.pageLoading.enable(false);
      } else { this.pageLoading.enable(true); }
      
      // Process data
      this.data = this.data.concat(this.processEvents(orders.data));
    })
    .catch((err) => {
      finishLoading();
      this.alert.create({
        title: 'Could not load events',
        subTitle: err,
        buttons: ['OK']
      }).present();
    });
  }

  /** Search next page of events.
   * @param indicator - Receives the loading indicator from Ionic. */
  searchMore(indicator) {
    // Add paging number
    this.searchParams.page++;

    // Execute search
    this.doSearch(this.searchParams);
  }

  /** Transform database events into a list grouped by date */
  processEvents(events) {
    // Order items by date group
    const groups = {};
    for (const event of events) {
      // Put event into correct group, by date
      const info = event.ts && event.ts.split(' ');
      const group = info[0];
      if (group) {
        if (!groups[group]) { groups[group] = []; }

        // Add extra fields
        event._summaryImg = '';
        // Find first product with an image
        for (const product of event.orderCartproduct) {
          if (product.orderCartproduct && product.orderCartproduct.product && product.orderCartproduct.product.imgUrl) {
            event._summaryImg = product.orderCartproduct.product.imgUrl;
            break;
          }
        }

        event._delivery = '';
        // Find first product with a delivery
        for (const product of event.orderCartproduct) {
          if (product.orderCartproduct && product.orderCartproduct.deliveryoption) {
            if (product.orderCartproduct.deliveryoption.id === '1') {
              // Set label for shipping delivery
              const data = product.orderCartproduct.deliveryoption.cartProductShippingdelivery;
              event._delivery = `Shipping to ${data.first_name} at ${data.street}, ${data.city}, ${data.state}, ${data.zip}, ${data.country}.`;
              break;
            }
          }
        }

        event._itemsAmount = event.orderCartproduct && event.orderCartproduct.length;
        event._value = parseFloat(event.netTotal || event.total).toFixed(2);
        
        event.total = parseFloat(event.total || 0).toFixed(2);
        event.netTotal = parseFloat(event.netTotal || 0).toFixed(2);
        event.tax = parseFloat(event.tax || 0).toFixed(2);
        event.shippingCharge = parseFloat(event.shippingCharge || 0).toFixed(2);

        groups[group].push(event);
      }
    }

    // Return groups as a ordered list
    const res = [];
    for (const group of Object.keys(groups)) {
      // Order events inside a day, by time
      groups[group].sort((a, b) => {
        if (a.ts < b.ts) { return 1; }
        if (a.ts > b.ts) { return -1; }
        return 0;
      });

      res.push({
        date: group,
        label: moment(group).calendar(null, {
          sameDay: '[Today]',
          nextDay: '[Tomorrow]',
          nextWeek: '[Next] dddd, MMM Do',
          sameElse: 'dddd, MMMM Do',

          lastDay: '[Yesterday]',
          lastWeek: '[Last] dddd, MMM Do',
        }),
        items: groups[group]
      });
    }

    // Order groups by date
    res.sort((a, b) => {
      if (a.date < b.date) { return 1; }
      if (a.date > b.date) { return -1; }
      return 0;
    });

    return res;
  }

  /** Show screen with event details */
  details(order) {
    this.nav.push(OrderDetailView, { order });
  }

}
