import {Component} from '@angular/core';
import {NavController, NavParams, ModalController, AlertController, Platform} from 'ionic-angular';
import {ImagePicker, ImagePickerOptions, Transfer} from 'ionic-native';

import {AccountService, APIRequest, Loading, SERVER_URL, ACCESS_TOKEN} from '../../utils';

declare const plugins: any;
declare const cordova: any;
declare const Zone: any;
let zoneToUpdatePhoto = null;
let zoneToUpdatePhotoThis = null;

@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Profile</ion-title>
  </ion-navbar>
</ion-header>

<ion-content padding>

  <ion-list class="gig-form">
    <ion-item>
      <ion-input type="text" [(ngModel)]="profile.first_name" placeholder="First Name"></ion-input>
    </ion-item>

    <ion-item>
      <ion-input type="text" [(ngModel)]="profile.last_name" placeholder="Last Name"></ion-input>
    </ion-item>

    <ion-item>
      <ion-input type="text" [(ngModel)]="profile.screen_name" placeholder="Username"></ion-input>
    </ion-item>

    <ion-item>
      <ion-input type="tel" [(ngModel)]="profile.phone" placeholder="Phone Number"></ion-input>
    </ion-item>

  </ion-list>

  <ion-list class="gig-form">
    <button ion-item (click)="changePhoto()">
      <ion-avatar item-left>
        <img src="{{profile.avatar_url || 'assets/avatar_placeholder.png'}}">
      </ion-avatar>
      Change Profile Photo
    </button>
  </ion-list>

  <button block large (click)="save()">SAVE</button>
</ion-content>
`,
})
export class ProfileView {

  profile: any = {};
  constructor(private account: AccountService,
              private modal: ModalController,
              private alert: AlertController,
              private platform: Platform,
              private nav: NavController) {

  }

  ionViewWillEnter() {
    // Get user data
    const data = this.account.getUserData() || {};
    if (data && data.profile) { this.profile = JSON.parse(JSON.stringify(data.profile)); }

    // Configure zone.js in case the photo is updated after using several plugins
    zoneToUpdatePhoto = Zone.current;
    zoneToUpdatePhotoThis = this;
  }

  changePhoto() {
    if (this.platform.is('android')) {
      // On Android, check permissions first
      const permissions = cordova.plugins.permissions;
      permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, (status) => {
        if (!status.hasPermission) {
          // Ask permission
          const errorCallback = () => {
            alert('Could not access your device photos.');
          };

          permissions.requestPermission(
            permissions.READ_EXTERNAL_STORAGE,
            (status) => {
              if (!status.hasPermission) {
                errorCallback();
              } else {
                // Sucess
                this.selectDevicePhoto();
              }
            },
            errorCallback);
        } else {
          // Permission ok, select photos
          this.selectDevicePhoto();
        }
      }, () => { alert('Could not access your device photos.'); });
    } else {
      // For other platforms, go straight
      this.selectDevicePhoto();
    }
  }

  selectDevicePhoto() {
    // Choose an image from device
    const options = {
      maximumImagesCount: 1,
      width: 300
    };

    ImagePicker.getPictures(options).then((results) => {
      // Sucess
      console.log(results);
      const photoUrl = results[0];

      if (photoUrl) {
        // Crop the image
        plugins.crop.promise(photoUrl, {})
        .then((croppedPhoto) => {
          // Upload image to server
          this.uploadFile(
            croppedPhoto,
            (response) => {
              // Get remote url
              const finalUrl = response && response.response && JSON.parse(response.response);

              zoneToUpdatePhoto.run(() => {
                // Update url
                if (finalUrl) {
                  zoneToUpdatePhotoThis.profile.avatar_url = 'assets/loading.gif'; // Add local loading icon first
                  setTimeout(() => { zoneToUpdatePhotoThis.profile.avatar_url = finalUrl; }, 500);
                } else {
                  zoneToUpdatePhotoThis.alert.create({
                    title: 'Could not finish uploading your photo.',
                    subTitle: 'The server did not generate a url',
                    buttons: ['OK']
                  }).present();
                }
              });
            },
            (err) => {
              console.log('error was', err);
              this.alert.create({
                title: 'Could not upload the image.',
                subTitle: `Error ${err.code || 0} - http ${err.http_status || 0}: ${err.body || 'no details'}`,
                buttons: ['OK']
              }).present();
            }
          );
        })
        .catch((err) => {
          console.log('error cropping', err);
        });
      } else {
        // No image was selected by user
        // Do nothing
      }
    }, (err) => {
      // Error
      console.log('Error', err);
    });
  }

  save() {
    // Store changes
    this.account.saveUserData({ profile: this.profile });

    // Close view
    this.nav.pop();
  }

  uploadFile(dataUrl: string, success, error) {
    const fileTransfer = new Transfer();
    const options = {
      fileKey: 'img',
      fileName: `${Math.random().toString(36).substring(2)}.jpg`,
      headers: {},
      chunkedMode: false,
    };

    Loading.showFullscreen();
    fileTransfer.upload(dataUrl, encodeURI(`${SERVER_URL}/images/?access-token=${ACCESS_TOKEN}`), options)
    .then((data) => {
      // success
      Loading.hideFullscreen();
      console.log('done uploading', data);
      success(data);
    }, (err) => {
      // error
      Loading.hideFullscreen();
      console.log('error uploading', err);
      error(err);
    });
  }

}