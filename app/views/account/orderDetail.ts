import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ValuesPipe} from '../../utils';

declare const moment: any;

@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Order Detail</ion-title>
  </ion-navbar>
</ion-header>

<ion-content>

  <ion-list class="events-list">

    <ion-list-header>
      <span item-left>Order #{{order.id}}</span>
      <span item-right>{{order.ts}}</span>
    </ion-list-header>

    <ion-list-header>Status: {{order.orderStatus || 'Unknown'}}</ion-list-header>
    <ion-list-header *ngIf="order.orderStatusNote">{{order.orderStatusNote}}</ion-list-header>

    <ion-list-header>Payment: {{order.paymentStatus || 'Unknown'}} ({{order.transactionProcessor}} {{order.transactionCode}})</ion-list-header>
    <ion-list-header *ngIf="order.paymentStatusNote">{{order.paymentStatusNote}}</ion-list-header>
    
    <ion-item-divider>Products</ion-item-divider>
    <ion-item *ngFor="let item of order.orderCartproduct">
      <ion-thumbnail item-left>
        <img src="{{(item.orderCartproduct && item.orderCartproduct.product && item.orderCartproduct.product.imgUrl) || 'assets/product_placeholder.png'}}">
      </ion-thumbnail>

      <h2 class="event-title">{{item.orderCartproduct && item.orderCartproduct.product && item.orderCartproduct.product.title}}</h2>
      
      <template [ngIf]="item.orderCartproduct && item.orderCartproduct.product && item.orderCartproduct.product.productVariantvalue && item.orderCartproduct.product.productVariantvalue.variantValueID">
        <p class="event-time" *ngFor="let variant of item.orderCartproduct.product.productVariantvalue.variantValueID | values">
      {{variant.variant.title}}: {{variant.title}}</p>
      </template>

      <p class="event-time" *ngIf="item.orderCartproduct">&#36;{{item.salePrice}}</p>
      <p class="event-time" *ngIf="item.orderCartproduct">Qty: {{item.orderCartproduct.qty}}</p>
      <p class="event-time">
        {{item.deliveryStatus}}
        <span *ngIf="item.deliveryStatusNote">- {{item.deliveryStatusNote}}</span>
      </p>
    </ion-item>

  </ion-list>

  <ion-list>
    <ion-item-divider>Cost</ion-item-divider>
    <ion-item>
      Subtotal <span item-right>&#36;{{order.total}}</span>
    </ion-item>

    <ion-item>
      Shipping <div item-right>&#36;{{order.shippingCharge || 0}}</div>
    </ion-item>

    <ion-item no-lines>
      Tax <div item-right>&#36;{{order.tax || 0}}</div>
    </ion-item>

    <ion-item class="cart-total">
      Grand Total <div item-right>&#36;{{order.netTotal || order.total || 0}}</div>
    </ion-item>
  </ion-list>

  <ion-list>
    <ion-item-divider>Delivery</ion-item-divider>
    <ion-item class="order-delivery-item">{{order._delivery || 'Could not find delivery information.'}}</ion-item>
  </ion-list>

</ion-content>
`,
  styles: [`
    .cart-total {
      font-weight: bold;
      border-top: 2pt solid black;
    }
  `],
  pipes: [ValuesPipe]
})
export class OrderDetailView {

  order;  
  constructor(private nav: NavController,
              private params: NavParams) {
    this.nav = nav;
    this.params = params;

    // Get event data from previous view
    this.order = this.params.get('order');
  }

}