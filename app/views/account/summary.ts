import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';

import {AccountService} from '../../utils';

import {SettingsView} from './settings';
import {AddressView} from './address';
import {OrdersView} from './orders';
import {ProfileView} from './profile';

@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>At The Gig</ion-title>
  </ion-navbar>
</ion-header>

<ion-content>

  <ion-list>
    
    <button ion-item (click)="editProfile()" class="profile-item">
      
      <ion-avatar item-left>
        <img src="{{profile.avatar_url || 'assets/avatar_placeholder.png'}}">
      </ion-avatar>

      <h2 *ngIf="profile.last_name || profile.first_name">{{profile.first_name}} {{profile.last_name}}</h2>
      <h2 *ngIf="!profile.last_name || !profile.first_name">Set your profile</h2>
      <p>{{profile.email}}</p>
    </button>

  </ion-list>

  <ion-list>
    <button ion-item (click)="orders()">
      <ion-icon name="time" item-left></ion-icon>
      Order History
    </button>

    <button ion-item (click)="shipping()">
      <ion-icon name="pin" item-left></ion-icon>
      Shipping Address
    </button>

    <button ion-item (click)="settings()">
      <ion-icon name="settings" item-left></ion-icon>
      Settings
    </button>

  </ion-list>

</ion-content>
`,
  styles: [`
    .profile-item h2 {
      margin-bottom: 5pt;
    }

    .profile-item ion-avatar {
      align-self: center;
    }
  `]
})
export class AccountView {

  profile: any = {};
  constructor(private account: AccountService,
              private nav: NavController) {

  }

  ionViewWillEnter() {
    // Get user data
    const data = this.account.getUserData() || {};
    this.profile = (data && data.profile) || {};
  }

  settings() {
    this.nav.push(SettingsView);
  }

  shipping() {
    this.nav.push(AddressView);
  }

  orders() {
    this.nav.push(OrdersView);
  }

  editProfile() {
    this.nav.push(ProfileView);
  }

}
