import {Component} from '@angular/core';
import {ModalController, NavController, AlertController} from 'ionic-angular';

import {AccountService} from '../../utils';

@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Shipping Address</ion-title>
  </ion-navbar>
</ion-header>

<ion-content padding>

  <ion-list class="gig-form">
    <ion-item>
      <ion-input required type="text" [(ngModel)]="name" placeholder="Recipient Name"></ion-input>
    </ion-item>

    <ion-item>
      <ion-input required type="text" [(ngModel)]="address" placeholder="Address (with Apt or Unit)"></ion-input>
    </ion-item>

    <ion-item>
      <ion-input required type="text" [(ngModel)]="zip" placeholder="ZIP"></ion-input>
    </ion-item>

    <ion-item>
      <ion-input required type="text" [(ngModel)]="city" placeholder="City"></ion-input>
    </ion-item>

    <ion-item>
      <ion-label>State</ion-label>
      <ion-select [(ngModel)]="state">
        <ion-option>Alabama</ion-option>
        <ion-option>Alaska</ion-option>
        <ion-option>Arizona</ion-option>
        <ion-option>Arkansas</ion-option>
        <ion-option>California</ion-option>
        <ion-option>Colorado</ion-option>
        <ion-option>Connecticut</ion-option>
        <ion-option>Delaware</ion-option>
        <ion-option>Florida</ion-option>
        <ion-option>Georgia</ion-option>
        <ion-option>Hawaii</ion-option>
        <ion-option>Idaho</ion-option>
        <ion-option>Illinois</ion-option>
        <ion-option>Indiana</ion-option>
        <ion-option>Iowa</ion-option>
        <ion-option>Kansas</ion-option>
        <ion-option>Kentucky</ion-option>
        <ion-option>Louisiana</ion-option>
        <ion-option>Maine</ion-option>
        <ion-option>Maryland</ion-option>
        <ion-option>Massachusetts</ion-option>
        <ion-option>Michigan</ion-option>
        <ion-option>Minnesota</ion-option>
        <ion-option>Mississippi</ion-option>
        <ion-option>Missouri</ion-option>
        <ion-option>Montana</ion-option>
        <ion-option>Nebraska</ion-option>
        <ion-option>Nevada</ion-option>
        <ion-option>New Hampshire</ion-option>
        <ion-option>New Jersey</ion-option>
        <ion-option>New Mexico</ion-option>
        <ion-option>New York</ion-option>
        <ion-option>North Carolina</ion-option>
        <ion-option>North Dakota</ion-option>
        <ion-option>Ohio</ion-option>
        <ion-option>Oklahoma</ion-option>
        <ion-option>Oregon</ion-option>
        <ion-option>Pennsylvania</ion-option>
        <ion-option>Rhode Island</ion-option>
        <ion-option>South Carolina</ion-option>
        <ion-option>South Dakota</ion-option>
        <ion-option>Tennessee</ion-option>
        <ion-option>Texas</ion-option>
        <ion-option>Utah</ion-option>
        <ion-option>Vermont</ion-option>
        <ion-option>Virginia</ion-option>
        <ion-option>Washington</ion-option>
        <ion-option>West Virginia</ion-option>
        <ion-option>Wisconsin</ion-option>
        <ion-option>Wyoming</ion-option>
      </ion-select>
    </ion-item>

  </ion-list>

  <button block large (click)="save()">SAVE ADDRESS</button>
</ion-content>
  `
})
export class AddressView {

  name;
  address;
  zip;
  city;
  state;
  constructor(private account: AccountService,
              private nav: NavController,
              private alert: AlertController) {
  }

  ngOnInit() {
    // Restore saved data
    const data = this.account.getUserData();
    if (data && data.address) {
      this.name = data.address.name;
      this.address = data.address.address;
      this.zip = data.address.zip;
      this.city = data.address.city;
      this.state = data.address.state;
    }
  }

  save() {
    // Check mandatory fields
    if (!this.name || !this.address || !this.zip || !this.city || !this.state) {
      this.alert.create({
        title: `Please fill all fields.`,
        buttons: ['OK'],
      }).present();
    } else {
      this.account.saveUserData({
        address: {
          name: this.name,
          address: this.address,
          zip: this.zip,
          city: this.city,
          state: this.state
        }
      });
      this.nav.pop();
    }
  }

}