import {Component} from '@angular/core';
import {NavController, AlertController} from 'ionic-angular';

import {APIRequest, Loading} from '../../utils';

@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title>Change Password</ion-title>
  </ion-navbar>
</ion-header>

<ion-content padding>
  
  <ion-list class="gig-form">
    <ion-item>
      <ion-input type="password" [(ngModel)]="current" placeholder="Current Password"></ion-input>
    </ion-item>
  </ion-list>

  <ion-list class="gig-form">
    <ion-item>
      <ion-input type="password" [(ngModel)]="next1" placeholder="New Password"></ion-input>
    </ion-item>

    <ion-item>
      <ion-input type="password" [(ngModel)]="next2" placeholder="New Password Again"></ion-input>
    </ion-item>
  </ion-list>

  <button block large (click)="save()">UPDATE PASSWORD</button>

</ion-content>
`,
})
export class PasswordView {

  current: string;
  next1: string;
  next2: string;
  constructor(private nav: NavController,
              private alert: AlertController,
              private api: APIRequest) {

  }

  save() {
    // Save new password
    if (this.current && this.next1 && this.next2 && this.next1 === this.next2) {
      // Send request
      Loading.showFullscreen();
      this.api.call('PUT', 'users', { password: this.next1 }).then(res => {
        // Confirm
        Loading.hideFullscreen();
        this.alert.create({
          title: 'Your password was changed.',
          buttons: ['OK'],
        }).present();

        // Close
        this.nav.pop(); 
      }).catch(err => {
        // Show error
        Loading.hideFullscreen();
        this.alert.create({
          title: 'Could not change your password.',
          subTitle: err,
          buttons: ['OK'],
        }).present();
      });
    } else {
      // Reset fields and try again
      this.current = '';
      this.next1 = '';
      this.next2 = '';

      this.alert.create({
        title: 'Please fill all fields.',
        buttons: ['OK'],
      }).present();
    }
  }

}
