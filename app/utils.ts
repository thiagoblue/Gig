import {Component, Injectable, Pipe, PipeTransform} from '@angular/core';
import {AlertController} from 'ionic-angular';
import {Push} from 'ionic-native';

import {TabsView} from './views/tabs';

// @BUG can not create a view correctly because it references the module who references it
// import {LoginView} from './views/login';

import { Http, RequestOptions,
         Request, RequestMethod, Headers } from '@angular/http';

declare const window: any;
declare const Zone: any;
declare const BraintreePlugin: any;


/* Stores user data */
export let ACCESS_TOKEN = localStorage.getItem('accessToken') || '';
let USER_POSITION = null;
let USER_DATA = JSON.parse(localStorage.getItem('userData')) || {};



/** Hides or display loading indicators */
export class Loading {

  /** Shows the global loading indicator, full screen. */
  static showFullscreen() {
    document.getElementById('gig_global_loading').style.display = 'block';
  }

  static hideFullscreen() {
    document.getElementById('gig_global_loading').style.display = 'none';
  }

}


/** Controls app behaviour based on an active event for entire app. */
let ACTIVE_EVENT: any = { event: null };
@Injectable()
export class AppNavigation {

  tabControl: any;

  getActiveEvent() {
    return ACTIVE_EVENT;
  }

  setActiveEvent(event) {
    ACTIVE_EVENT.event = event;
  }

}


let ACTIVE_VIEW: any = null;
/** Controls the main screen shown. */
@Injectable()
export class RouterService {

  /** Set which screen is shown. */
  setMainView(aView: any) {
    ACTIVE_VIEW = aView;
  }

  /** Return the screen being show. */
  getActiveView(): any {
    return ACTIVE_VIEW;
  }

}

export const SERVER_URL = 'https://aspen-insurance.us/at-the-gig/api/web/v1';
/** Handle external requests to API. */
@Injectable()
export class APIRequest {

  constructor(private http: Http) {
  }

  call(method: string = 'GET', path: string, params: Object = {}, returnFullErrors = false): Promise<any> {
    return new Promise((resolve, reject) => {
      // Makes a request for external API

      let encodedParams = '';
      if (method === 'GET' || method === 'DELETE') {
        // Convert object into url params
        const paramsList = [];
        for (const key of Object.keys(params)) {
          paramsList.push(`${key}=${encodeURIComponent(params[key])}`);
        }
        encodedParams = paramsList.join('&');
      }

      // Set headers
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');

      // Choose proper method
      let methodSpec = null;
      switch (method) {
        case 'GET': methodSpec = RequestMethod.Get; break;
        case 'POST': methodSpec = RequestMethod.Post; break;
        case 'PUT': methodSpec = RequestMethod.Put; break;
        case 'DELETE': methodSpec = RequestMethod.Delete; break;
        default: methodSpec = RequestMethod.Get; break;
      }

      const req: any = {
        method: methodSpec,
        url: `${SERVER_URL}/${path}/?${ (ACCESS_TOKEN && 'access-token=' + ACCESS_TOKEN) || '' }`,
        headers,
        withCredentials: false,
      };
      if (method === 'POST' || method === 'PUT') {
        // Included JSON body
        req.body = JSON.stringify(params);
      } else if (encodedParams) {
        // Append params to the url
        req.url += `&${encodedParams}`;
      }
      const options = new RequestOptions(req);

      this.http.request(new Request(options))
      .subscribe(
        res => {
          // Success
          let response = null;
          
          // Try to parse JSON
          try {
            response = res.json();
          } catch (error) {}
            
          // Return response
          resolve(response);
        },
        err => {
          // Error
          if (err.status === 401) {
            // User must logout
            AccountService.forceLogout();
          }

          // Build friendly error message
          const messages = [];
          
          try {
            const error = JSON.parse(err._body);
            
            for (const key of Object.keys(error)) {
              messages.push(error[key]);
            }
          } catch (jsonError) {
            messages.push('Unknown error');
          }
          
          console.log('Error was', err, messages);
          if (returnFullErrors) {
            // Return complete error message
            reject({
              error: err,
              message: messages.join(' ')
            });
          } else {
            // Return just a simple error message
            reject(messages.join(' '));
          }
        }
      );
    });
  }

}




let CART = {
  products: null,
  external: null,
  delivery: null,
  interface: { hasJustAddedProduct: false }
};
/** Manages cart and checkout */
@Injectable()
export class CartService {

  constructor(private api: APIRequest,
              private http: Http) {}

  /** Returns current cart. */
  getCart() {
    return CART;
  }

  reset() {
    CART.products = null;
    CART.external = null;
    CART.delivery = null;
  }

  refresh(): Promise<any> {
    return new Promise((refreshResolve, refreshReject) => {
      if (CART.external && CART.external.id) {
        // Refresh cart data
        Loading.showFullscreen();
        this.api.call('GET', `carts/${CART.external.id}`).then(res => {
          // Store new cart data
          CART.external = res;

          // Fix missing decimal place in API ($10.20 comes as 10.2)
          const fixes = ['tax', 'total', 'netTotal', 'shippingCharge'];
          for (const fix of fixes) {
            if (res[fix]) { res[fix] = parseFloat(res[fix]).toFixed(2); }
          }

          Loading.hideFullscreen();
          refreshResolve();
        }).catch(err => { refreshReject(err); });
      } else { refreshResolve(); }
    });
  };

  /** Configure shipping address */
  setDelivery(type = 'shipping', content?, reload = true): Promise<any> {
    return new Promise((resolve, reject) => {      
      if (type === 'shipping') {
        // Update every product
        let tasks = 0;
        const maybeFinish = () => {
          // Check if all pending tasks are complete
          if (!tasks || tasks <= 0) {
            CART.delivery = { type, content };
            resolve(true);
            
            if (reload) { this.refresh(); }
            return;
          }
        };

        for (const key of Object.keys(CART.products)) {
          const product = CART.products[key];
          tasks++;

          // Update delivery for this item
          this.api.call('PUT', `carts/products/${product._updateID}`, {
            deliveryoptionID: 1,
            cartproductShippingdelivery: {
              street: content.address,
              city: content.city,
              state:  content.state,
              country: 'USA',
              zip: content.zip,
              first_name: content.name,
            }
          }).then(res => {
            // Finish task
            tasks--;
            maybeFinish();
          }).catch(err => { reject(err); });
        }
      }

    });
  }

  /** Add, update or remove product in cart. */
  setProduct(item): Promise<any> {
    return new Promise((resolve, reject) => {
      // Note how many tasks this item has to process
      let tasks = 0;

      const createCart = () => {
        // Create a new empty cart and proceed
        this.api.call('POST', 'carts').then(res => {
          // Success, continue processing item
          CART.external = res;
          CART.products = {};
          processItem();
        }).catch(err => {
          // Cancel operation
          reject(err);
          return;
        });
      };

      const addProduct = (product) => {
        // Add a product to a cart
        const params: any = { qty: product._amount };

        if (product._amount <= 0) {
          // Finish task
          tasks--;
          maybeFinish();
          return;
        }

        // Find product id
        params.productID = product._cartID;
        if (product._extraCartID) { params.productVariantvalueID = product._extraCartID; }

        // Maybe add updated address
        if (USER_DATA && USER_DATA.address) {
          const address = USER_DATA.address;

          params.deliveryoptionID = 1;
          params.cartproductShippingdelivery = {
            street: address.address,
            city: address.city,
            zip: address.zip,
            state: address.state,
            first_name: address.name,
            country: 'USA',
          };
          
          CART.delivery = {
            type: 'shipping',
            content: address
          };
        }

        this.api.call('POST', `carts/${CART.external.id}/products`, params).then(res => {
          // Add product to cart
          product._updateID = res.id;
          CART.products[ getInternalCartID(product) ] = product;


          // Finish task
          tasks--;
          maybeFinish();
        }).catch(err => {
          // Cancel
          reject(err);
          return;
        });
      };

      const removeProduct = (product) => {
        // Remove product from cart
        this.api.call('DELETE', `carts/products/${product._updateID}`).then(res => {
          // Remove product from cart
          delete CART.products[ getInternalCartID(product) ];

          // Finish task
          tasks--;
          maybeFinish();
        }).catch(err => {
          // Cancel
          reject(err);
          return;
        });
      };

      const updateProduct = (product) => {
        // Change product quantity
        this.api.call('PUT', `carts/products/${product._updateID}`, {
          qty: product._amount
        }).then(res => {
          // Update product in cart
          CART.products[ getInternalCartID(product) ] = product;

          // Finish task
          tasks--;
          maybeFinish();
        }).catch(err => {
          // Cancel
          reject(err);
          return;
        });
      };

      const maybeFinish = () => {
        // Check if all pending tasks are complete
        if (!tasks || tasks <= 0) {
          // Refresh cart data
          this.refresh()
          .then(() => {
            // Finish
            resolve(true);
          }).catch(err => { reject(err); });
        }
      };

      const processItem = () => {
        // Take actions for each product
        for (const product of item._options) {
          tasks++;
          
          if (CART.products[ getInternalCartID(product) ]) {
            // If product already exists on cart, update it
            if (product._amount <= 0) {
              // Remove product from cart
              removeProduct(product);
            } else {
              // Edit product
              updateProduct(product);
            }
          } else {
            // Add a new product to cart
            addProduct(product);
          }
        }
      };

      const getInternalCartID = (product) => {
        return product._cartID + (product._extraCartID && `_${product._extraCartID}` || '');
      };

      if (!CART.products) {
        // If there is no cart, create one and then continue
        createCart();
      } else {
        // Process item
        processItem();
      }
    });
  }

  /** Prepares order, show payment screen and confirm order when done. */
  checkout(): Promise<any> {
    return new Promise((resolve, reject) => {
      // Define several steps

      const prepareBraintreePayment = () => {
        return new Promise((tokenResolve, tokenReject) => {
          // Get a token for payment processor
          this.api.call('POST', 'orders/braintree-client-token')
            .then(token => { tokenResolve(token); })
            .catch(err => { tokenReject(err); });
        });
      };

      const doBraintreePayment = (token) => {
        return new Promise((paymentResolve, paymentReject) => {
          // Use token with payment processor
          if (BraintreePlugin) {
            // Initialize plugin
            const ref = this;
            const zone = Zone.current;

            BraintreePlugin.initialize(token,
              () => {
                // Open payment screen
                const options = {
                  cancelText: 'Cancel',
                  title: 'Purchase',
                  ctaText: 'Select Payment Method',
                  amount: '$' + parseFloat(CART.external.netTotal).toFixed(2),
                  primaryDescription: 'At The Gig Events\' Products',
                  secondaryDescription: 'Shipping and taxes included.'
                };
                BraintreePlugin.presentDropInPaymentUI(options, (result) => {
                  if (result.userCancelled) {
                    // Canceled by user
                    zone.run(function () { paymentReject('You cancelled the payment'); });
                  } else {
                    // Success, continue
                    zone.run(function () {
                      console.info('User completed payment dialog.', result);
                      paymentResolve(result.nonce);
                    });
                  }
                });
              },
              (err) => { zone.run(function () { paymentReject(err); }); }
            );
          } else {
            paymentReject('Your device does not support the payment feature.');
          }
        });
      };

      const sendOrder = (braintreePaymentNonce) => {
        return new Promise((orderResolve, orderReject) => {
          // Create order
          this.api.call('POST', `orders/carts/${CART.external.id}`, {
            braintreePaymentMethodNonce: braintreePaymentNonce
          })
          .then(res => { orderResolve(res); })
          .catch(err => { orderReject(err); });
        });
      };

      const finishCheckout = (orderResult) => {
        // Check success. Finish all and show order ID.
        
        // Clean cart
        this.reset();

        // Return order ID
        resolve(orderResult);
      };

      
      // Initiate all necessary steps to checkout
      
      // Get token to connect to external payment processor
      prepareBraintreePayment()
      .then(token => {
        // Connect to external payment processor
        doBraintreePayment(token)
        .then(nonce => {
          // Payment done. Send order to API, which will finish the charge
          sendOrder(nonce)
          .then(res => {
            // All done. Finish checkout.
            finishCheckout(res);
          })
          .catch(err => { reject(err); });
        })
        .catch(err => { reject(err); });
      })
      .catch(err => { reject(err); });
    });
  }

}




/** Manages user account. */
@Injectable()
export class AccountService {

  platform: string = 'web'; // Platform which requests are being made
  constructor(private router: RouterService,
              private api: APIRequest,
              private alert: AlertController,
              private cartServ: CartService) {
  }

  /** Returns the last detected user GPS position. */
  getUserPosition(): Promise<any> {
    return new Promise((resolve, reject) => {
      // Update user position
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            // Store position
            USER_POSITION = {
              lat: position.coords.latitude,
              long: position.coords.longitude
            };

            // Return it
            resolve(USER_POSITION);
          },
          (err) => {
            console.log(err);
            reject(err);
          },
          {
            maximumAge: 1000 * 60 * 10, // Accept positions up to X miliseconds old
            timeout: 5000, // Return error if taking more than those miliseconds
            enableHighAccuracy: false,
          }
        );
      } else {
        reject('Geolocation not supported');
      }
    });
  }

  /** Check if user is logged in or not. */
  isLoggedIn(): boolean {
    const isLoggedIn = ACCESS_TOKEN !== '';

    // Configure push
    if (isLoggedIn) { this.configurePush(); }

    // Any criteria may cancel logged in status
    return isLoggedIn;
  }

  /** Returns access token to use when talking to API. */
  getToken(): string {
    return ACCESS_TOKEN;
  }

  /** Initiates login and choose next screen. */
  login(service: string, info?: any) {
    // Store current zone
    const zone = Zone.current;
    const ref = this;
    
    // Handle a sucess external login
    const loginSuccess = (identifier: string, token: string, profile: any) => {
      console.log(91, identifier, token, profile);

      // Finish login process
      const loginFinished = (token: string) => {
        // Execute through zone.js, so changes will be detected after a external login called back
        zone.run(() => {
          if (token) {
            // Store token
            localStorage.setItem('accessToken', token);
            ACCESS_TOKEN = token;

            // Store profile
            if (profile) {
              this.saveUserData({ profile: profile.profile });
            }

            // Sync user settings
            this.loadRemoteData();

            // Login is success, open next view
            Loading.hideFullscreen();
            ref.router.setMainView(TabsView);

            // Configure push
            this.configurePush();
          } else {
            // Show Error
            Loading.hideFullscreen();
            this.alert.create({
              title: `Could not login.`,
              subTitle: 'Please try again.',
              buttons: ['OK'],
            }).present();
          }
        });
      };

      // Register an account
      const registerUser = (profile) => {
        // Add platform to profile
        profile.login = { loggedInApp: this.platform };

        this.api.call('POST', 'users', profile)
        .then(res => {
          // Extract access token and continue
          const token = res.login && res.login['access-token'];
          loginFinished(token);
        })
        .catch(err => {
          // Cancel login
          Loading.hideFullscreen();
          this.alert.create({
            title: `Could not register your account.`,
            subTitle: err,
            buttons: ['OK'],
          }).present();
        });
      };
      
      // Try to login using provided data
      let params: any = {};
      if (service === 'email') {
        // Set login data using email and password
        params = {
          email: identifier,
          password: token,
          loggedInApp: this.platform
        };
      } else {
        // Set login data for social network
        params = { identifier, loggedInApp: this.platform };
      }

      // Skip if email registration was asked instead
      if (service === 'email' && identifier === 'register') {
        // Register by email
        registerUser(profile);
        return;
      }
      
      // Login now
      this.api.call('GET', 'users/login', params, true)
      .then(token => {
        // Finish login
        loginFinished(token);
      })
      .catch(error => {
        const err = error.error || {};

        if (service !== 'email' && (err.status === 404 || err.status === 401)) {
          // If login failed, register the user
          registerUser(profile);
        } else {
          // Show another Error
          Loading.hideFullscreen();
          this.alert.create({
            title: `Could not login.`,
            subTitle: err,
            buttons: ['OK'],
          }).present();
        }
      });
    };

    // Handle a failed external login
    const loginFail = () => {
      Loading.hideFullscreen();
    };

    // Show loading screen
    Loading.showFullscreen();

    if (service === 'google') {
      // Log ins using Google
      if (window.plugins && window.plugins.googleplus) {
        
        // Function to handle response
        const handleSuccess = (userData) => {
          // Make a standard object with user's info
          const identifier = userData.userId;
          const token = userData.idToken || userData.accessToken || '';

          // Convert Google's DisplayName into first and last name
          const values = userData.displayName.split(' ');
          const first_name = values[0];
          values.splice(0, 1);
          const last_name = values.join(' ');

          const profile = {
            email: userData.email,
            profile: {
              first_name,
              last_name,
              screen_name: userData.displayName,
              avatar_url: userData.imageUrl,
            },
            socialprofile: {
              identifier,
              provider: 'gplus',
              profile: userData
            }
          };

          // Call sucess callback
          loginSuccess(identifier, token, profile);
        };


        // Check login state
        window.plugins.googleplus.trySilentLogin(
          {},
          handleSuccess,
          (err) => {
            // Needs to ask for permissions
            window.plugins.googleplus.login(
              {},
              handleSuccess,
              (err) => {
                console.log(err);
                loginFail();
              }
            );
          }
        );
      } else {
        loginFail();
        this.alert.create({
          title: 'Your device does not support this feature.',
          buttons: ['OK'],
        }).present();
      }
    } else if (service === 'DEBUG') {
      // Use debug option to login
      const identifier = prompt('type FB identifier to use'); // '10154358395193080';
      const profile = {
        email: prompt('email to use'), // 'thyako@gmail.com',
        profile: {
          first_name: 'Test',
          last_name: 'Fake User',
        },
        socialprofile: {
          identifier,
          provider: 'fb',
          profile: { fakedata: 'test account'},
        }
      };

      loginSuccess(identifier, '', profile);
    } else if (service === 'email') {
      // Login or register by email
      if (info.register) {
        // Do registration
        loginSuccess('register', null, {
          email: info.email,
          password: info.password,
          profile: {
            email: info.email,
            first_name: info.firstName,
            last_name: info.lastName,
          }
        });
      } else {
        // Do login
        loginSuccess(info.email, info.password, { email: info.email });
      }
    } else {
      // Defaults to login using Facebook
      if (window.facebookConnectPlugin) {

        // Function to handle response
        const handleResponse = (userData) => {
          if (userData.status === 'connected' && userData.authResponse) {
            // User is logged in
            handleSuccess(userData);
          } else {
            // User needs to give permissions
            askPermissions();
          }
        };

        const askPermissions = () => {
          // User must give permissions
          window.facebookConnectPlugin.login(
            ['public_profile', 'email', 'user_friends'],
            handleSuccess,
            (err) => {
              console.log(err);
              loginFail();
            }
          );
        };

        const handleSuccess = (userData) => {
          // Make a standard object with user's info
          const identifier = userData.authResponse.userID;
          const token = userData.authResponse.accessToken;
          const profile = {
            email: null,
            profile: null,
            socialprofile: {
              identifier,
              provider: 'fb',
              profile: null,
            }
          };

          // Get more profile info
          window.facebookConnectPlugin.api(
            'me/?fields=email,first_name,last_name,gender,age_range,picture',
            ['public_profile', 'email', 'user_friends'],
            (res) => {
              // Add more info to profile
              profile.email = res.email;
              profile.profile = {
                first_name: res.first_name,
                last_name: res.last_name,
                screen_name: res.first_name,
                avatar_url: (res.picture && res.picture.data && res.picture.data.url) || null,
              };

              // Store all returned data in socialprofile field
              profile.socialprofile.profile = res;

              // Call sucess callback
              loginSuccess(identifier, token, profile);
            },
            (err) => {
              loginFail();
              this.alert.create({
                title: `Could not access your data`,
                subTitle: err,
                buttons: ['OK'],
              }).present();
            }
          );
        };

        // Check login status first
        window.facebookConnectPlugin.getLoginStatus(handleResponse, askPermissions);
      } else {
        loginFail();
        this.alert.create({
          title: `Your device does not support this feature.`,
          buttons: ['OK'],
        }).present();
      }
    }
  }

  /** Logs out the user and go to login screen. */
  logout() {
    Loading.showFullscreen();

    const finishLogout = () => {
      // Remove login credentials
      localStorage.clear();
      ACCESS_TOKEN = '';

      // Go to login screen
      // @BUG cannot instantiate a view with cyclic reference
      // this.router.setMainView(LoginView);
      window.location.reload();
    };

    // Ask server to logout
    this.api.call('DELETE', `users/${ACCESS_TOKEN}/logout`).then(finishLogout).catch(finishLogout);
  }

  static forceLogout() {
    // Logs out without using the server api
    localStorage.clear();
    window.location.reload();
  }

  loadRemoteData() {
    // Fetch user settings from server
    this.api.call('GET', 'userSettings').then(res => {
      // Convert to local values
      const final = {};
      for (const category of Object.keys(res)) {
        final[category] = {};

        for (const setting of Object.keys(res[category])) {
          final[category][setting] = res[category][setting].value;
        }
      }

      // Save
      this.saveUserData({ settings: final });
    }).catch(err => {
      // Show error
      this.alert.create({
        title: 'Could load your settings from server.',
        subTitle: err,
        buttons: ['OK'],
      }).present();
    });

    // Fetch user profile from server
    this.api.call('GET', 'users').then(res => {
      if (res && res.profile) {
        // Update profile
        const profile = { email: res.email };
        Object.assign(profile, res.profile);

        // Save
        this.saveUserData({ profile }, true);
      }
    }).catch(err => {
      // Show error
      this.alert.create({
        title: 'Could load your profile from server.',
        subTitle: err,
        buttons: ['OK'],
      }).present();
    });
  }

  getUserData() {
    return USER_DATA;
  }

  saveUserData(data, skipRemoteUpdate = false) {
    // Update shipping info if it changed
    const prev = USER_DATA && USER_DATA.address;
    const changed = (data && data.address && prev !== data.address);

    // Save new user data
    for (const key of Object.keys(data)) { USER_DATA[key] = data[key]; }    
    localStorage.setItem('userData', JSON.stringify(USER_DATA));

    // Trigger cart update if shipping address changed
    if (CART.products && changed && USER_DATA.address) {
      this.cartServ.setDelivery('shipping', USER_DATA.address).catch(err => {
        alert('Error setting your delivery option: ' + err);
      });
    }

    // If profile was updated, also save on server side
    if (data.profile && !skipRemoteUpdate) {
      Loading.showFullscreen();
      this.api.call('PUT', 'users', {
        profile: {
          avatar_url: data.profile.avatar_url,
          first_name: data.profile.first_name,
          last_name: data.profile.last_name,
          screen_name: data.profile.screen_name,
          phone: data.profile.phone,
        }
      })
      .then(res => { Loading.hideFullscreen(); })
      .catch(err => { Loading.hideFullscreen(); });
    }
  }

  configurePush() {
    // Configure push notifications
    const push = Push.init({
      android: {
        senderID: '776308221837'
      },
      ios: {
        alert: true,
        badge: true,
        sound: true
      }
    });

    try {
      push.on('registration', (data) => {
        console.log('PUSH TOKEN RECEIVED', data.registrationId);
      });

      push.on('error', (e) => {
        console.log('PUSH ERROR', e);
      });
    } catch (err) { console.log('Could not set push', err); }
  }
  
}



/** Converts an object into an array. */
@Pipe({
  name: 'values',
  pure: false
})
export class ValuesPipe implements PipeTransform {
  transform(value: any, args?: any[]): any[] {
    // create instance vars to store keys and final output
    let keyArr: any[] = Object.keys(value), dataArr = [];

    // loop through the object,
    // pushing values to the return array
    keyArr.forEach((key: any) => {
      dataArr.push(value[key]);
    });

    // return the resulting array
    return dataArr;
  }
}